#include <iostream>

#include <ged/debug/profiling.h>
#include <ged/core/common.h>
#include <ged/core/gedmath.h>
#include <ged/core/gedtime.h>
#include <ged/core/input.h>
#include <ged/core/stringid.h>

#include <ged/gl/glgraphicsdevice.h>
#include <ged/gl/glbasicrenderer.h>

#include <ged/gameobject/gameobject.h>
#include <ged/model.h>
#include <ged/multimodelloader.h>
#include <ged/camera.h>
#include <ged/transform.h>

#include <ged/event/event.h>
#include <ged/event/eventcomponent.h>
#include <ged/event/eventqueue.h>

#include <ged/processor/fpscameraprocessor.h>
#include <ged/global.h>

int main()
{
    /*** Systems ***/
    
    // Create all systems
    ged::gl::GraphicsDevice gfx_device;
    ged::gl::BasicRenderer model_renderer;
    ged::InputManager input_mgr;
    ged::EventQueue event_queue;

    // Create GraphicsConfig
    ged::GraphicsConfig cfg;
    cfg.resolution_x = 1280;
    cfg.resolution_y = 720;
    cfg.vertical_sync = 0;

    // Initialize Systems
    gfx_device.Init(cfg);
    model_renderer.Init(gfx_device);
    input_mgr.Init(gfx_device);

    /*** Resources ***/
    
    // Sponza models
    std::string sponza_file = ged::global::RESOURCE_BASE_PATH + "models/sponza/sponza.gmdl";
    std::vector<ged::Model*> sponza_models = LoadMultiModel(gfx_device, sponza_file);

    // Bob model
    std::string bob_file = ged::global::RESOURCE_BASE_PATH + "models/bob/bob.gmdl";
    ged::Model* bob_mdl = LoadModel(gfx_device, bob_file);

    // Crate model
    std::string crate_file = ged::global::RESOURCE_BASE_PATH + "models/crate/crate.gmdl";
    ged::Model* crate_mdl = LoadModel(gfx_device, crate_file);

    // Barrel model
    std::string barrel_file = ged::global::RESOURCE_BASE_PATH + "models/barrel/barrel.gmdl";
    ged::Model* barrel_mdl = LoadModel(gfx_device, barrel_file);

    /*** GameObjects ***/
    std::vector<ged::GameObject> objects;

    // Sponza scene
    for (size_t i = 0; i < sponza_models.size(); i++)
    {
        ged::Model* mdl = sponza_models[i];
        ged::GameObject obj = ged::GameObject::Create();
        obj.SetComponent<ged::RenderComponent>(mdl);
        obj.SetComponent<ged::TransformComponent>(ged::Transform());
    }

    // Bob actor
    ged::GameObject bob = ged::GameObject::Create();
    bob.SetComponent<ged::RenderComponent>(bob_mdl);
    bob.SetComponent<ged::TransformComponent>(
        ged::Transform(ged::vec3(0, 0.2f, 0),
                       ged::quat(ged::vec3(-ged::PI_HALF, ged::PI_HALF, 0)),
                       ged::vec3(0.02f)));

    // Crate
    ged::GameObject obj = ged::GameObject::Create();
    obj.SetComponent<ged::RenderComponent>(crate_mdl);
    obj.SetComponent<ged::TransformComponent>(ged::Transform(ged::vec3(-2.f, 0.5f, 0.f)));

    // Barrel
    ged::GameObject barrel = ged::GameObject::Create();
    barrel.SetComponent<ged::RenderComponent>(barrel_mdl);
    barrel.SetComponent<ged::TransformComponent>(ged::Transform(ged::vec3(-2.f, 0.0f, -1.2f)));

    // Setup Camera Parameters
    ged::CameraParameters cam_param;
    cam_param.width = cfg.resolution_x;
    cam_param.height = cfg.resolution_y;

    // Create a camera using ged::camera factory
    ged::GameObject camera = ged::camera::Create(cam_param, ged::Transform(ged::vec3(5, 2, 0)));
    camera.SetComponent<ged::EventComponent>(std::make_shared<ged::FPSCameraProcessor>());
   
    // Fill object vector with all active GameObjects
    objects.clear();
    for (ged::GameObject go : ged::GameObject::GetManager())
        objects.push_back(go);

    const float PHYSICS_STEP = 1.f / 60.f;
    float physics_accumulator = 0.f;

    // 'Main Loop'
    while (!input_mgr.KeyDown(ged::KEY_ESCAPE))
    {
        // This updates the global frame time measure which can be read from ged::GetFrameTime()
        ged::UpdateFrameTime();
        ged::f32 dt = ged::min(1.f / 30.f, ged::GetFrameTime());
        physics_accumulator += dt;
        
        // Update input
        input_mgr.UpdateStatesAndCreateEvents(&event_queue);
        
        // Process events
        event_queue.ProcessEvents();

        // Render
        gfx_device.ClearBuffers(ged::GED_ALL_BUFFERS);
        model_renderer.Render(objects, camera.GetComponent<ged::CameraComponent>()->GetParameters());

        // Swap front and back buffers
        gfx_device.SwapBuffers();
    }

    // Delete resources;
    for (ged::Model* mdl : sponza_models)
        delete mdl;
    delete bob_mdl;
    delete crate_mdl;
    delete barrel_mdl;

    // Shutdown of systems
    input_mgr.Shutdown();
    model_renderer.Shutdown();
    gfx_device.Shutdown();

    return 0;
}
