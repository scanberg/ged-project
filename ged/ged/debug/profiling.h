#pragma once

#include <unordered_map>
#include "../core/common.h"
#include "../core/stringid.h"
#include "../core/gedtime.h"

namespace ged
{
    void RegisterProcessName(StringID process, const char* name);
    void RegisterProcessParent(StringID process, StringID parent);
    void PrintProfilingReport();

    class ExplicitProfiler
    {
    public:
        ExplicitProfiler(StringID process) :
            m_process(process)
        {}

        inline void Start()
        {
            m_start_time = GetTimeStamp();
        }

        void Stop();

    private:
        TimeStamp m_start_time;
        StringID m_process;
    };

    class ScopedProfiler
    {
    public:
        ScopedProfiler(StringID process) :
            m_start_time(GetTimeStamp()),
            m_process(process)
        {}

        ~ScopedProfiler();
    private:
        TimeStamp m_start_time;
        StringID m_process;
    };
}