#include "profiling.h"
#include <string>

struct ProfilingItem
{
    ProfilingItem() :
        tot_time(0),
        count(0)
    {}

    ged::u64 tot_time;
    ged::u64 count;
};

// static const ged::StringID NO_PARENT = 0;

static std::unordered_map<ged::StringID, std::string> process_name_map{};
static std::unordered_map<ged::StringID, ged::StringID> process_parent_map{};
static std::unordered_map<ged::StringID, ProfilingItem> profiling_map{};

namespace ged
{
    void RegisterProcessName(StringID process, const char* name)
    {
        process_name_map[process] = std::string(name);
    }

    void RegisterProcessParent(StringID process, StringID parent)
    {
        process_parent_map[process] = parent;
    }

    void PrintProfilingReport()
    {
        // TODO Print parental hierarchy for profiled processes
        /*
        std::unordered_map<ged::StringID, bool> process_printed;
        for (auto p_item : profiling_map)
            process_printed[p_item.first] = false;
         */

        printf("\n---------- Profiling Report (in ms) ----------\n");
        printf("Process                  tot time     avg time\n");
        printf("----------------------------------------------\n");

        for (auto it : profiling_map)
        {
            StringID id = it.first;
            u64 tot_time = it.second.tot_time / 1000000;
            f64 avg_time = tot_time / static_cast<f64>(it.second.count);
            std::string str = "PROCESS " + std::to_string(id);
            
            auto name_it = process_name_map.find(id);
            if (name_it != process_name_map.end())
                str = name_it->second;

            printf("%-20s %12llu %12.5f\n", str.c_str(), tot_time, avg_time);
        }
        printf("------------ End Profiling Report ------------\n\n");
    }

    void ExplicitProfiler::Stop()
    {
        TimeStamp end_time = GetTimeStamp();
        u64 dt = GetDeltaTimeNanoSeconds(m_start_time, end_time);
        auto& p_item = profiling_map[m_process];
        p_item.tot_time += dt;
        p_item.count++;
    }

    ScopedProfiler::~ScopedProfiler()
    {
        TimeStamp end_time = GetTimeStamp();
        u64 dt = GetDeltaTimeNanoSeconds(m_start_time, end_time);
        auto& p_item = profiling_map[m_process];
        p_item.tot_time += dt;
        p_item.count++;
    }
}