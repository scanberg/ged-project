#pragma once
#include <cassert>
#include <map>
#include <vector>
#include "core/common.h"
#include "core/gedmath.h"
#include "graphics/graphicsdevice.h"

namespace ged
{
	class Texture;

    const i32 MAX_MATERIAL_COMPONENTS = 8;

	enum class PBRMaterialComponent
	{
        GED_BASE_COLOR,
        GED_NORMAL,
        GED_ALPHA,
        GED_SMOOTHNESS,
        GED_REFLECTANCE,
        GED_METAL_MASK,
        GED_DETAIL,
        GED_EMISSION,
        GED_UNDEFINED
    };

	class Material
	{
	public:
		struct Component
		{
            Component(const Texture* tex = nullptr, const vec4& col = vec4(1)) :
				texture(tex), color(col) {}

            const Texture* texture;
			vec4 color;
		};

		inline void SetComponent(i32 component_idx, const Component& component)
		{
            assert(component_idx > -1 && component_idx < MAX_MATERIAL_COMPONENTS);
			m_components[component_idx] = component;
		}

        inline void SetComponent(PBRMaterialComponent component_id, const Component& component)
        {
            SetComponent(static_cast<i32>(component_id), component);
        }

        inline const Component& GetComponent(i32 component_idx) const
        {
            assert(component_idx > -1 && component_idx < MAX_MATERIAL_COMPONENTS);
            return m_components[component_idx];
        }

        inline const Component& GetComponent(PBRMaterialComponent component_id) const
        {
            return GetComponent(static_cast<i32>(component_id));
        }

	private:
		Component m_components[MAX_MATERIAL_COMPONENTS];
	};

    std::vector<Material*> LoadMaterials(const GraphicsDevice& gfx_dev, const char* filename, std::map<std::string, size_t>* mat_id = nullptr);
    inline std::vector<Material*> LoadMaterials(const GraphicsDevice& gfx_dev, const std::string& filename, std::map<std::string, size_t>* mat_id = nullptr)
    {
        return LoadMaterials(gfx_dev, filename.c_str(), mat_id);
    }
}