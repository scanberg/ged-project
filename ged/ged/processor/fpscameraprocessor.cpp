#include "fpscameraprocessor.h"
#include "../core/gedmath.h"
#include "../core/gedtime.h"
#include "../core/input.h"
#include "../transform.h"

namespace ged
{
    void FPSCameraProcessor::Process(const std::shared_ptr<Event>& event_ptr, GameObject target, EventQueue*)
    {
        auto tc = target.GetComponent<TransformComponent>();
        if (tc == nullptr) {
            //TODO log error
            printf("FPS Camera Processor target lacks TransformComponent!\n");
            return;
        }

        if (event_ptr->id == KEYBOARD_EVENT_ID)
        {
            // Safe to cast because ID matched
            KeyboardEvent* ke = reinterpret_cast<KeyboardEvent*>(event_ptr.get());
            
            if (ke->action == ACTION_HIT || ke->action == ACTION_HOLD)
            {
                f32 dt = GetFrameTime();
                f32 speed = 3.f * dt;

                vec3 move;
                if (ke->key == KEY_W)
                    move.z -= speed;
                if (ke->key == KEY_S)
                    move.z += speed;
                if (ke->key == KEY_A)
                    move.x -= speed;
                if (ke->key == KEY_D)
                    move.x += speed;

                tc->transform.Move(move);
            }
        }
        else if (event_ptr->id == MOUSE_MOVE_EVENT_ID)
        {
            // "Safe" to cast here
            MouseMoveEvent* mme = reinterpret_cast<MouseMoveEvent*>(event_ptr.get());
            vec2 mouse_delta = mme->position - mme->prev_position;

            yaw -= mouse_delta.x * 0.001f;
            pitch -= mouse_delta.y * 0.001f;

            pitch = clamp(pitch, -PI_HALF, PI_HALF);
            
            if (yaw < -TWO_PI)
                yaw += TWO_PI;
            if (yaw > TWO_PI)
                yaw -= TWO_PI;

            quat pitch_quat(vec3(pitch, 0, 0));
            quat yaw_quat(vec3(0, yaw, 0));

            quat& rot = tc->transform.rotation;
            rot = yaw_quat * pitch_quat;
        }
    }
}