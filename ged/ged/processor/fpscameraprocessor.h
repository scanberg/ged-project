#pragma once

#include "../core/common.h"
#include "../core/input.h"
#include "../event/eventprocessor.h"

namespace ged
{
    struct FPSCameraProcessor : public EventProcessor
    {
        f32 pitch, yaw;
        FPSCameraProcessor() :
            EventProcessor({ KEYBOARD_EVENT_ID, MOUSE_MOVE_EVENT_ID }),
            pitch(0.f),
            yaw(0.f)
        {}

        void Process(const std::shared_ptr<Event>& event_ptr, GameObject target, EventQueue* queue) override;
    };
}