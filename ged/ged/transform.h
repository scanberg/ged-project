#pragma once

#include <glm/gtx/transform.hpp>
#include "core/gedmath.h"
#include "gameobject/gameobject.h"

namespace ged
{
	// A very modest transform object class
	struct Transform
	{
		static Transform Mix(const Transform& t0, const Transform& t1, float t);
		static Transform LookAt(const vec3& eye, const vec3& point, const vec3& up = vec3(0, 1, 0));

		Transform(const vec3& pos = vec3(0.0f), const quat& rot = quat(), const vec3& scl = vec3(1.0f));
		Transform(const Transform& other) = default;
		Transform(const mat3& m);
		Transform(const mat4& m);

		void Translate(const vec3& v);
		void Move(const vec3& v);
		void Rotate(const quat& q);
		void Rotate(const vec3& euler);

		mat4 Matrix() const;
		mat4 InvMatrix() const;
		vec3 EulerAngles() const;

		quat rotation;
		vec3 translation;
		vec3 scale;
	};

    struct TransformComponent : public Component<TransformComponent>
    {
        TransformComponent(const Transform& t = Transform()) :
            transform(t) {}

        inline operator const Transform& () const
        {
            return transform;
        }

        Transform transform;
    };

	inline Transform::Transform(const vec3& trans, const quat& rot, const vec3& scl) :
		rotation(rot), translation(trans), scale(scl) {}

	inline Transform::Transform(const glm::mat3& m)
	{
		rotation = glm::quat_cast(m);
		translation = vec3(0);
		scale = vec3(length(m[0]),
					 length(m[1]),
					 length(m[2]));
	}

	inline Transform::Transform(const glm::mat4& m)
	{
		rotation = glm::quat_cast(m);
		translation = vec3(m[3]);
		scale = vec3(length(glm::vec3(m[0])),
					 length(glm::vec3(m[1])),
					 length(glm::vec3(m[2])));
	}

	inline void Transform::Translate(const glm::vec3& v)
	{
		translation += v;
	}

	inline void Transform::Move(const glm::vec3& v)
	{
		translation += glm::mat3_cast(rotation) * v;
	}

	inline void Transform::Rotate(const glm::quat& q)
	{
		rotation = rotation * q;
	}

	inline void Transform::Rotate(const glm::vec3& euler)
	{
		rotation = rotation * quat(vec3(euler.y, euler.x, euler.z));
	}

	inline mat4 Transform::Matrix() const
	{
		const mat4 T = glm::translate(translation);
		const mat4 R = glm::mat4_cast(rotation);
		const mat4 S = glm::scale(scale);
		return T * R * S;
	}

	inline mat4 Transform::InvMatrix() const
	{
		//assert(glm::all(glm::greaterThan(glm::abs(scale), vec3(0.001f))));
		const mat4 T = glm::translate(-translation);
		const mat4 R = glm::mat4_cast(glm::conjugate(rotation));
		const mat4 S = glm::scale(1.0f / scale);
		return S * R * T;
	}

	inline vec3 Transform::EulerAngles() const
	{
		return glm::eulerAngles(rotation);
	}

	inline Transform Transform::Mix(const Transform& t0, const Transform &t1, float t)
	{
		t = clamp(t, 0.0f, 1.0f);
		return Transform(mix(t0.translation, t1.translation, t),
			             mix(t0.rotation, t1.rotation, t),
			             mix(t0.scale, t1.scale, t));
	}

	inline Transform Transform::LookAt(const vec3& eye, const vec3& point, const vec3& up)
	{
		quat q = glm::conjugate(glm::quat_cast(glm::lookAt(eye, point, up)));
		return Transform(eye, q);
	}
}
