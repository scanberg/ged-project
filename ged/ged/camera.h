#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include "core/common.h"
#include "core/gedmath.h"
#include "gameobject/gameobject.h"
#include "transform.h"

// Undefine stupid Windows.h defines
#ifdef near
#undef near
#endif

#ifdef far
#undef far
#endif

namespace ged
{
    enum class ProjectionMode
    {
        GED_PERSPECTIVE_PROJECTION,
        GED_ORTHOGRAPHIC_PROJECTION
    };

    struct CameraParameters
    {
        CameraParameters() :
            width(1024),
            height(768),
            near(0.1f),
            far(1000.f),
            fovy(PI_THIRD),
            fstop(2.4f),
            exposure(1.f)
        {}

        mat4 world_to_view_matrix;
        mat4 proj_matrix;

        u32 width;
        u32 height;

        f32 near;
        f32 far;
        f32 fovy;
        f32 fstop;
        f32 exposure;
    };

	struct CameraComponent : public Component<CameraComponent>
	{
		CameraComponent(const CameraParameters& param = CameraParameters()) :
            param(param)
		{}

		float AspectRatio() const;
        
		mat4 GetProjectionMatrix(ProjectionMode mode = ProjectionMode::GED_PERSPECTIVE_PROJECTION) const;
		mat4 GetWorldToViewMatrix() const;
        mat4 GetViewToWorldMatrix() const;

        const CameraParameters& GetParameters();

    	CameraParameters param;
	};

    inline float CameraComponent::AspectRatio() const {
        return (float)param.width / (float)param.height;
    }

    inline mat4 CameraComponent::GetProjectionMatrix(ProjectionMode mode) const {
        if (mode == ProjectionMode::GED_PERSPECTIVE_PROJECTION)
            return glm::perspective(param.fovy, AspectRatio(), param.near, param.far);
        else
            return glm::ortho(0.f, (float)param.width, (float)param.height, 0.f, param.near, param.far);
    }

    inline mat4 CameraComponent::GetWorldToViewMatrix() const {
        const TransformComponent* tc = GetGameObject().GetComponent<TransformComponent>();
        assert(tc);
        return tc->transform.InvMatrix();
    }

    inline mat4 CameraComponent::GetViewToWorldMatrix() const {
        const TransformComponent* tc = GetGameObject().GetComponent<TransformComponent>();
        assert(tc);
        return tc->transform.Matrix();
    }

    inline const CameraParameters& CameraComponent::GetParameters() {
        // Update matrices
        param.world_to_view_matrix = GetWorldToViewMatrix();
        param.proj_matrix = GetProjectionMatrix();

        return param;
    }

	namespace camera
	{
		inline GameObject Create(const CameraParameters& param = CameraParameters(), const Transform& trans = Transform()) {
			GameObject go = GameObject::Create();
			go.SetComponent<CameraComponent>(param);
            go.SetComponent<TransformComponent>(trans);
            return go;
		}
	};
}