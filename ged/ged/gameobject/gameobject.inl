#pragma once

namespace ged
{
    template<class T>
    inline u32 Component<T>::family()
    {
        static u32 i = s_family_counter++;
        assert(i < MAX_COMPONENTS);
        return i;
    }

    template<class T>
    inline GameObject Component<T>::GetGameObject() const
    {
        return m_gameobject;
    }

    template<class T>
    inline void ComponentManager<T>::Destroy(GameObjectManager* gom, GameObject go)
    {
        gom->RemoveComponent<T>(go);
    }

    template<class T, typename... Args>
    inline dependant_type<BaseComponent,T,void> GameObject::SetComponent(Args&&... args)
    {
        s_mgr.SetComponent<T>(*this, std::forward<Args>(args)...);
    }

    template<class T>
    inline dependant_type<BaseComponent,T,T*> GameObject::GetComponent()
    {
        return s_mgr.GetComponent<T>(*this);
    }

    template<class T>
    inline dependant_type<BaseComponent,T,bool> GameObject::HasComponent()
    {
        return s_mgr.HasComponent<T>(*this);
    }

    template<class T>
    inline dependant_type<BaseComponent,T,void> GameObject::RemoveComponent()
    {
        s_mgr.RemoveComponent<T>(*this);
    }

    inline bool GameObject::IsValid() const
    {
        return s_mgr.IsValid(*this);
    }

    inline bool GameObject::operator == (const GameObject& other)
    {
        return m_handle == other.m_handle;
    }

    inline bool GameObject::operator != (const GameObject& other)
    {
        return m_handle != other.m_handle;
    }

    inline bool GameObject::operator < (const GameObject& other)
    {
        return m_handle.index() < other.m_handle.index();
    }

    inline bool GameObject::operator > (const GameObject& other)
    {
        return m_handle.index() > other.m_handle.index();
    }

    inline GameObject GameObject::Create()
    {
        return s_mgr.Create();
    }

    inline void GameObject::Destroy(GameObject& go)
    {
        return s_mgr.Destroy(go);
    }

    inline GameObjectManager& GameObject::GetManager()
    {
        return s_mgr;
    }

    inline const GameObjectManager& GameObject::GetConstManager()
    {
        return s_mgr;
    }

    inline GameObjectManager::iterator::iterator(size_t idx) :
        m_idx(idx)
    {}

    inline bool GameObjectManager::iterator::operator == (const GameObjectManager::iterator& other) const
    {
        return m_idx == other.m_idx;
    }

    inline bool GameObjectManager::iterator::operator != (const GameObjectManager::iterator& other) const
    {
        return m_idx != other.m_idx;
    }

    inline GameObjectManager::iterator& GameObjectManager::iterator::operator++()
    {
        const std::vector<GameObject>& objects = GameObject::GetConstManager().m_game_objects;
        ++m_idx;
        while (m_idx < objects.size() && objects[m_idx].m_handle == INVALID_HANDLE)
            ++m_idx;
        
        return *this;
    }

    inline GameObjectManager::iterator GameObjectManager::iterator::operator++(int)
    {
        const std::vector<GameObject>& objects = GameObject::GetConstManager().m_game_objects;
        iterator it = *this;
        ++m_idx;
        while (m_idx < objects.size() && objects[m_idx].m_handle == INVALID_HANDLE)
            ++m_idx;

        return it;
    }

    inline GameObject& GameObjectManager::iterator::operator*() const
    {
        std::vector<GameObject>& objects = GameObject::GetManager().m_game_objects;
        return objects[m_idx];
    }

    inline GameObject* GameObjectManager::iterator::operator->() const
    {
        std::vector<GameObject>& objects = GameObject::GetManager().m_game_objects;
        return &objects[m_idx];
    }

    // Const iterator

    inline GameObjectManager::const_iterator::const_iterator(size_t idx) :
        m_idx(idx)
    {}

    inline bool GameObjectManager::const_iterator::operator == (const GameObjectManager::const_iterator& other) const
    {
        return m_idx == other.m_idx;
    }

    inline bool GameObjectManager::const_iterator::operator != (const GameObjectManager::const_iterator& other) const
    {
        return m_idx != other.m_idx;
    }

    inline GameObjectManager::const_iterator& GameObjectManager::const_iterator::operator++()
    {
        const std::vector<GameObject>& objects = GameObject::GetConstManager().m_game_objects;
        ++m_idx;
        while (m_idx < objects.size() && objects[m_idx].m_handle == INVALID_HANDLE)
            ++m_idx;

        return *this;
    }

    inline GameObjectManager::const_iterator GameObjectManager::const_iterator::operator++(int)
    {
        const std::vector<GameObject>& objects = GameObject::GetConstManager().m_game_objects;
        const_iterator it = *this;
        ++m_idx;
        while (m_idx < objects.size() && objects[m_idx].m_handle == INVALID_HANDLE)
            ++m_idx;

        return it;
    }

    inline const GameObject& GameObjectManager::const_iterator::operator*() const
    {
        const std::vector<GameObject>& objects = GameObject::GetConstManager().m_game_objects;
        return objects[m_idx];
    }

    inline const GameObject* GameObjectManager::const_iterator::operator->() const
    {
        const std::vector<GameObject>& objects = GameObject::GetConstManager().m_game_objects;
        return &objects[m_idx];
    }

    inline GameObjectManager::GameObjectManager() :
        m_size(0)
    {
        m_data.reserve(GAME_OBJECT_RESERVE_SIZE);
        m_game_objects.reserve(GAME_OBJECT_RESERVE_SIZE);
        m_free_slots.reserve(GAME_OBJECT_RESERVE_SIZE);

        for (u32 i = 0; i < MAX_COMPONENTS; i++)
            m_component_managers[i] = nullptr;
    }

    inline GameObjectManager::iterator GameObjectManager::begin()
    {
        return GameObjectManager::iterator();
    }

    inline GameObjectManager::iterator GameObjectManager::end()
    {
        const std::vector<GameObject>& objects = m_game_objects;
        return GameObjectManager::iterator(objects.size());
    }

    inline GameObjectManager::const_iterator GameObjectManager::begin() const
    {
        return GameObjectManager::const_iterator();
    }

    inline GameObjectManager::const_iterator GameObjectManager::end() const
    {
        const std::vector<GameObject>& objects = m_game_objects;
        return GameObjectManager::const_iterator(objects.size());
    }

    inline GameObjectManager::const_iterator GameObjectManager::cbegin() const
    {
        return GameObjectManager::const_iterator();
    }

    inline GameObjectManager::const_iterator GameObjectManager::cend() const
    {
        const std::vector<GameObject>& objects = m_game_objects;
        return GameObjectManager::const_iterator(objects.size());
    }

    inline size_t GameObjectManager::size()
    {
        return m_size;
    }

    inline bool GameObjectManager::IsValid(GameObject go)
    {
        u32 index = m_handle_mgr.Get(go.m_handle);
        return m_game_objects[index].m_handle == go.m_handle;
    }

    /* OK */
    inline GameObject GameObjectManager::Create()
    {
        // Check if we have some available slots
        if (m_free_slots.size() > 0)
        {
            // Just update the underlying data of this position
            u32 index = m_free_slots.back();
            m_free_slots.pop_back();
            m_data[index] = GameObjectData();
            m_game_objects[index].m_handle = m_handle_mgr.Add(index);

            m_size++;
            return m_game_objects[index];
        }

        // Otherwise, just allocate a new slot
        u32 index = static_cast<u32>(m_data.size());
        m_data.emplace_back();
        m_game_objects.emplace_back();
        m_game_objects.back().m_handle = m_handle_mgr.Add(index);

        m_size++;
        return m_game_objects.back();
    }

    inline void GameObjectManager::Destroy(GameObject& go)
    {
        assert(IsValid(go));

        Handle handle = go.m_handle;
        u32 idx = m_handle_mgr.Get(handle);
        for (u32 i = 0; i < MAX_COMPONENTS; i++)
        {
            if (m_data[idx].component_handles[i] != INVALID_HANDLE)
                m_component_managers[i]->Destroy(this, go);
        }

        // Add to free slots
        m_free_slots.emplace_back(idx);

        // Free up handle
        m_handle_mgr.Remove(handle);

        // Invalidate internal handle
        m_game_objects[idx].m_handle = INVALID_HANDLE;

        // Invalidate passed handle
        handle = INVALID_HANDLE;

        m_size--;
    }

    template<class T>
    inline void GameObjectManager::AccomodateComponent()
    {
        u32 family = T::family();
        if (m_component_managers[family])
            return;

        ComponentManager<T>* c_mgr = new ComponentManager<T>();
        c_mgr->m_components.reserve(COMPONENT_RESERVE_SIZE);
        m_component_managers[family] = c_mgr;
    }

    /* Verify  */
    template<class T>
    inline T* GameObjectManager::GetComponent(GameObject go)
    {
        assert(IsValid(go));

        u32 family = T::family();
        if (!m_component_managers[family])
            return nullptr;

        u32 go_idx = m_handle_mgr.Get(go.m_handle);
        Handle c_hand = m_data[go_idx].component_handles[family];

        if (c_hand == INVALID_HANDLE)
            return nullptr;

        u32 c_idx = m_handle_mgr.Get(c_hand);

        ComponentManager<T>* c_mgr = reinterpret_cast<ComponentManager<T>*>(m_component_managers[family]);
        return &c_mgr->m_components[c_idx];
    }

    template<class T>
    inline bool GameObjectManager::HasComponent(GameObject go)
    {
        assert(IsValid(go));
        u32 family = T::family();
        u32 go_idx = m_handle_mgr.Get(go.m_handle);
        return m_component_managers[family] &&
               m_data[go_idx].component_handles[family] != INVALID_HANDLE;
    }

    template<class T, typename... Args>
    inline void GameObjectManager::SetComponent(GameObject go, Args&&... args)
    {
        assert(IsValid(go));
        AccomodateComponent<T>();

        u32 family = T::family();

        u32 go_idx = m_handle_mgr.Get(go.m_handle);
        Handle c_hand = m_data[go_idx].component_handles[family];
        ComponentManager<T>* c_mgr = reinterpret_cast<ComponentManager<T>*>(m_component_managers[family]);
        
        if (c_hand != INVALID_HANDLE)
        {
            // This component exists already, do nothing!
            // TODO throw warning
            return;
        }
        
        // No previous data for this component was found
        // Allocate a slot for it at index c_idx
        u32 c_idx = 0;
        
        if (c_mgr->m_free_slots.size() > 0)
        {
            // Take one available slot and pop
            c_idx = c_mgr->m_free_slots.back();
            c_mgr->m_free_slots.pop_back();
            new (&c_mgr->m_components[c_idx]) T(std::forward<Args>(args)...);
        }
        else
        {
            // Allocate a new slot
            c_idx = static_cast<u32>(c_mgr->m_components.size());
            c_mgr->m_components.emplace_back(std::forward<Args>(args)...);
        }
            
        // Update GO handle in component and component handles
        c_mgr->m_components[c_idx].m_gameobject.m_handle = go.m_handle;
        c_hand = m_handle_mgr.Add(c_idx);
        m_data[go_idx].component_handles[family] = c_hand;
    }

    template<class T>
    inline void GameObjectManager::RemoveComponent(GameObject go)
    {
        assert(IsValid(go));

        u32 family = T::family();
        assert(family < MAX_COMPONENTS);

        if (!m_component_managers[family])
        {
            // No component manager exists for this component?!
            // TODO: Warn or fail
            return;
        }

        u32 go_idx = m_handle_mgr.Get(go.m_handle);
        Handle c_hand = m_data[go_idx].component_handles[family];

        if (c_hand == INVALID_HANDLE)
        {
            // Handle was invalid for component
            // TODO: WARN
            return;
        }

        u32 c_idx = m_handle_mgr.Get(c_hand);
        ComponentManager<T>* c_mgr = reinterpret_cast<ComponentManager<T>*>(m_component_managers[family]);
        
        // Remove component handle
        m_handle_mgr.Remove(c_hand);
        
        // Call destructor of component
        c_mgr->m_components[c_idx].~T();
        
        // Add index to free list
        c_mgr->m_free_slots.emplace_back(c_idx);
        
        // Invalidate handle
        m_data[go_idx].component_handles[family] = INVALID_HANDLE;

/*
        u32 c_last = static_cast<u32>(c_mgr->m_components.size() - 1);
        if (c_idx != c_last)
        {
            c_mgr->m_components[c_idx] = c_mgr->m_components[c_last];

            // Update GameObjects component handle of this moved component
            Handle moved_go = c_mgr->m_components[c_idx].m_gameobject.m_handle;
            u32 moved_idx = m_handle_mgr.Get(moved_go);

            Handle moved_c = m_data[moved_idx].component_handles[family];
            m_handle_mgr.Update(moved_c, c_idx);
        }

        c_mgr->m_components.back().~T();
        c_mgr->m_components.pop_back();
        m_handle_mgr.Destroy(c_hand);
        m_data[go_idx].component_handles[family] = INVALID_HANDLE;
 */
    }
}
