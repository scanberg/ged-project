//
//  handlemanager.h
//  ged
//
//  Created by Robin Skånberg on 2015-08-22.
//  Copyright (c) 2015 Visual Communications Group Ulm. All rights reserved.
//
//  Modified version of handlemanager presented here:
//  http://gamesfromwithin.com/managing-data-relationships
//
//  Copyright (c) 2008, Power of Two Games LLC
//  All rights reserved.

#pragma once

#include <vector>
#include "handle.h"

namespace ged
{
    class HandleManager
    {
    public:
        enum{ MAX_ENTRIES = (1 << 22) };

        HandleManager();

        Handle Add(u32 index);
        void Update(Handle handle, u32 value);
        void Remove(Handle handle);

        u32 Get(Handle handle) const;

        size_t size() const;
        size_t capacity() const;
        void clear();

    private:
        void Grow();

        HandleManager(const HandleManager&);
        HandleManager& operator=(const HandleManager&);

        struct HandleEntry
        {
            HandleEntry()
                : m_next_free_index(0)
                , m_version(1)
                , m_active(0)
                , m_end_of_list(0)
            {}

            explicit HandleEntry(u32 next_free_index)
                : m_next_free_index(next_free_index)
                , m_version(1)
                , m_active(0)
                , m_end_of_list(0)
            {}

            u32 m_next_free_index : 22;
            u32 m_version : 8;
            bool m_active : 1;
            bool m_end_of_list : 1;
            u32 m_entry;
        };

        std::vector<HandleEntry> m_entries;

        u32 m_active_entry_count;
        u32 m_first_free_entry;
    };

    inline u32 HandleManager::Get(Handle handle) const
    {
        const u32 index = handle.m_index;
        assert(m_entries[index].m_version == handle.m_version);
        assert(m_entries[index].m_active);
        return m_entries[index].m_entry;
    }

    inline size_t HandleManager::size() const
    {
        return m_active_entry_count;
    }

    inline size_t HandleManager::capacity() const
    {
        return m_entries.size();
    }
}
