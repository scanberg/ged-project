#include <cassert>
#include "handlemanager.h"

namespace ged
{
    HandleManager::HandleManager()
    {
        clear();
    }

    void HandleManager::Grow()
    {
        const int GROW_SIZE = 256;

        u32 start = static_cast<u32>(m_entries.size());
        u32 end = start + GROW_SIZE;

        m_entries.resize(end);

        for (u32 i = start; i < end - 1; ++i)
            m_entries[i] = HandleEntry(i + 1);
        m_entries[end - 1] = HandleEntry();
        m_entries[end - 1].m_end_of_list = true;

        if (start > 0) {
            m_entries[start - 1].m_end_of_list = false;
            m_entries[start - 1].m_next_free_index = start;
        }

    }

    void HandleManager::clear()
    {
        m_active_entry_count = 0;
        m_first_free_entry = 0;

        m_entries.clear();
        Grow();
    }

    Handle HandleManager::Add(u32 value)
    {
        if (m_active_entry_count >= capacity() - 1)
            Grow();

        const u32 new_index = m_first_free_entry;
        assert(new_index < MAX_ENTRIES);
        assert(m_entries[new_index].m_active == false);
        assert(!m_entries[new_index].m_end_of_list);

        m_first_free_entry = m_entries[new_index].m_next_free_index;
        m_entries[new_index].m_next_free_index = 0;
        m_entries[new_index].m_version = (m_entries[new_index].m_version + 1);
        if (m_entries[new_index].m_version == 0)
            m_entries[new_index].m_version = 1;
        m_entries[new_index].m_active = true;
        m_entries[new_index].m_entry = value;

        ++m_active_entry_count;

        return Handle(new_index, m_entries[new_index].m_version);
    }

    void HandleManager::Update(Handle handle, u32 value)
    {
        const int index = handle.m_index;
        assert(m_entries[index].m_version == handle.m_version);
        assert(m_entries[index].m_active == true);

        m_entries[index].m_entry = value;
    }

    void HandleManager::Remove(Handle handle)
    {
        const u32 index = handle.m_index;
        assert(m_entries[index].m_version == handle.m_version);
        assert(m_entries[index].m_active == true);

        m_entries[index].m_next_free_index = m_first_free_entry;
        m_entries[index].m_active = false;
        m_first_free_entry = index;

        --m_active_entry_count;
    }
}
