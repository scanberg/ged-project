/*

GameObject Manager system

Inspired by EntityX:
https://github.com/alecthomas/entityx/tree/master/entityx

And Randy Gaul:
http://www.randygaul.net/2013/05/20/component-based-engine-design/

*/

#pragma once

#include <cstring>
#include <cassert>
#include <iterator>
#include <vector>
#include "../core/common.h"
#include "../core/dependant_type.h"
#include "handlemanager.h"

namespace ged
{
    // Constants
    const u32 MAX_COMPONENTS = 32U;
    const u32 GAME_OBJECT_RESERVE_SIZE = 1024U;
    const u32 COMPONENT_RESERVE_SIZE = 64U;

    // Pre-declaration of classes
    class BaseComponent;
    class BaseComponentManager;
    template<class T>
    class Component;
    template<class T>
    class ComponentManager;
    class GameObject;
    class GameObjectManager;

    /*!
     * \class BaseComponent
     *
     * \brief Base class for components, stores a static counter
     * that gives unique indices for every new Component class
     *
     * \author Skanberg
     * \date oktober 2015
     */
    class BaseComponent
    {
    protected:
        static u32 s_family_counter;
    };

    /*!
     * \class GameObject
     *
     * \brief The core object of the game engine.
     *
     * \author Skanberg
     * \date oktober 2015
     */
    class GameObject
    {
    public:
        template<class T, typename... Args>
        dependant_type<BaseComponent,T,void> SetComponent(Args&&... args);

        template<class T>
        dependant_type<BaseComponent,T,T*> GetComponent();

        template<class T>
        dependant_type<BaseComponent,T,bool> HasComponent();

        template<class T>
        dependant_type<BaseComponent,T,void> RemoveComponent();

        // Returns true if GameObject is valid
        bool IsValid() const;

        // Comparison operators (compares handle)
        bool operator != (const GameObject& other);
        bool operator == (const GameObject& other);
        bool operator < (const GameObject& other);
        bool operator > (const GameObject& other);

        static GameObject Create();
        static void Destroy(GameObject& go);

        static GameObjectManager& GetManager();
        static const GameObjectManager& GetConstManager();
    private:
        friend class GameObjectManager;

        static GameObjectManager s_mgr;
        Handle m_handle;
    };

    /*!
     * \class BaseComponentManager
     *
     * \brief The base class for a component manager.
     *
     * \author Skanberg
     * \date oktober 2015
     */
    class BaseComponentManager
    {
    public:
        virtual void Destroy(GameObjectManager* gom, GameObject go) = 0;
    };

    /*!
     * \class ComponentManager
     *
     * \brief Templated class for a manager of components of type T.
     *
     * \author Skanberg
     * \date oktober 2015
     */
    template<class T>
    class ComponentManager : public BaseComponentManager
    {
    public:
        void Destroy(GameObjectManager* gom, GameObject go) final;
        std::vector<T> m_components;
        std::vector<u32> m_free_slots;
    };

    /*!
     * \class Component
     *
     * \brief The Templated core component that can be used to define new components for GameObjects.
     *
     * \author Skanberg
     * \date oktober 2015
     */
    template<class T>
    class Component : public BaseComponent
    {
    public:
        static u32 family();
        GameObject GetGameObject() const;

    private:
        friend class GameObjectManager;
        GameObject m_gameobject;
    };

    /*!
     * \class GameObjectManager
     *
     * \brief The Manager for all GameObjects.
     *
     * \author Skanberg
     * \date oktober 2015
     */
    class GameObjectManager
    {
    public:
        class iterator : public std::iterator<std::forward_iterator_tag, GameObject>
        {
        public:
            iterator(size_t idx = 0);
            iterator(const iterator&) = default;
            ~iterator() = default;

            iterator& operator=(const iterator&) = default;
            bool operator==(const iterator&) const;
            bool operator!=(const iterator&) const;

            iterator& operator++();
            iterator operator++(int);

            GameObject& operator*() const;
            GameObject* operator->() const;
        private:
            size_t m_idx;
        };

        class const_iterator : public std::iterator<std::forward_iterator_tag, GameObject>
        {
        public:
            const_iterator(size_t idx = 0);
            const_iterator(const const_iterator&) = default;
            ~const_iterator() = default;

            const_iterator& operator=(const const_iterator&) = default;
            bool operator==(const const_iterator&) const;
            bool operator!=(const const_iterator&) const;

            const_iterator& operator++();
            const_iterator operator++(int);

            const GameObject& operator*() const;
            const GameObject* operator->() const;
        private:
            size_t m_idx;
        };

        iterator begin();
        iterator end();

        const_iterator begin() const;
        const_iterator end() const;

        const_iterator cbegin() const;
        const_iterator cend() const;

        size_t size();
        
        GameObject Create();
        void Destroy(GameObject& handle);

        bool IsValid(GameObject handle);

    private:
        friend class GameObject;
        template<class T>
        friend class ComponentManager;

        struct GameObjectData
        {
            GameObjectData() {}
            GameObjectData(const GameObjectData& other)
            {
                memcpy(component_handles, other.component_handles, sizeof(component_handles));
            }

            Handle component_handles[MAX_COMPONENTS];
        };

        // Hide constructor
        GameObjectManager();

        template<class T>
        void AccomodateComponent();

        template<class T>
        T* GetComponent(GameObject go);

        template<class T>
        bool HasComponent(GameObject go);

        template<class T, typename... Args>
        void SetComponent(GameObject go, Args&&... args);

        template<class T>
        void RemoveComponent(GameObject go);

        size_t m_size;

        std::vector<GameObjectData> m_data;
        std::vector<GameObject> m_game_objects;
        std::vector<u32> m_free_slots;

        BaseComponentManager* m_component_managers[MAX_COMPONENTS];
        HandleManager m_handle_mgr;
    };

    const GameObject INVALID_GAMEOBJECT = GameObject();
}

#include "gameobject.inl"