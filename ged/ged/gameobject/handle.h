#pragma once

#include "../core/common.h"

namespace ged
{
    class Handle
    {
    public:
        Handle(u32 index = 0, u32 version = 0) :
            m_index(index),
            m_version(version)
        {}

        Handle(const Handle& other) = default;
        ~Handle() = default;

        inline operator u32() const
        {
            return m_index | m_version << 24;
        }

        inline u32 index() const
        {
            return m_index;
        }

        inline u32 version() const
        {
            return m_version;
        }

    private:
        friend class HandleManager;
        u32 m_index : 24;
        u32 m_version : 8;
    };

    const Handle INVALID_HANDLE = Handle();
}
