#pragma once

// God damn windows defines
#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#ifdef abs
#undef abs
#endif

#include "../core/common.h"
#include "../core/gedmath.h"

namespace ged
{
    struct AABB
    {
        vec3 min_box;
        vec3 max_box;

        AABB(const vec3& min_val = vec3(0), const vec3& max_val = vec3(0)) :
            min_box(min_val), max_box(max_val)
        {}

        AABB(const vec3 points[], size_t point_count)
        {
            if (point_count == 0)
                return;

            min_box = vec3(FLT_MAX);
            max_box = vec3(-FLT_MAX);

            for (size_t i = 0; i < point_count; i++)
            {
                min_box = min(min_box, points[i]);
                max_box = max(max_box, points[i]);
            }
        }

        vec3 size() const
        {
            return abs(max_box - min_box);
        }

        // TODO Implement
        /* Scale operators */
        AABB operator * (f32 scale) const;
        AABB operator * (const vec3& scale) const;
        AABB operator / (f32 inv_scale) const;
        AABB operator / (const vec3& inv_scale) const;

        /* Offset operators */
        AABB operator + (const vec3& offset) const;
        AABB operator - (const vec3& offset) const;

        /* Union */
        AABB operator + (const AABB& other) const;
        /* Difference */
        AABB operator - (const AABB& other) const;
    };

    inline AABB AABB::operator * (f32 scale) const
    {
        return AABB(min_box * scale, max_box * scale);
    }

    inline AABB AABB::operator * (const vec3& scale) const
    {
        return AABB(min_box * scale, max_box * scale);
    }

    inline AABB AABB::operator / (f32 inv_scale) const
    {
        return AABB(min_box / inv_scale, max_box / inv_scale);
    }

    inline AABB AABB::operator / (const vec3& inv_scale) const
    {
        return AABB(min_box / inv_scale, max_box / inv_scale);
    }

    inline AABB AABB::operator + (const vec3& offset) const
    {
        return AABB(min_box + offset, max_box + offset);
    }

    inline AABB AABB::operator - (const vec3& offset) const
    {
        return AABB(min_box - offset, max_box - offset);
    }

    inline AABB AABB::operator + (const AABB& other) const
    {
        return AABB(min(min_box, other.min_box), max(max_box, other.max_box));
    }

    inline AABB AABB::operator - (const AABB& other) const
    {
        AABB aabb(vec3(-FLT_MAX), vec3(FLT_MAX));
        aabb.min_box = max(aabb.min_box, max(min_box, other.min_box));
        aabb.max_box = min(aabb.max_box, min(max_box, other.max_box));

        return aabb;
    }
}
