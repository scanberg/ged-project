#pragma once
#include "../core/common.h"
#include "../core/gedmath.h"

namespace ged
{
    struct Sphere
    {
        vec3 position;
        f32 radius;

        Sphere(const vec3& position = vec3(0), f32 radius = 0) :
            position(position), radius(radius)
        {}

        Sphere(const vec4& geom) :
            position(vec3(geom)), radius(geom.w)
        {}
        
        operator vec4() const
        {
            return vec4(position, radius);
        }
    };
}