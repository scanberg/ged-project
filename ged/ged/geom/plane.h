#pragma once
#include "../core/common.h"
#include "../core/gedmath.h"

namespace ged
{
    struct Plane
    {
        f32 x, y, z, w;

        Plane(f32 x = 0, f32 y = 0, f32 z = 0, f32 w = 0) :
            x(x), y(y), z(z), w(w)
        {}

        Plane(const vec4& v) :
            x(v.x), y(v.y), z(v.z), w(v.w)
        {}

    };
}