#pragma once
#include "../core/gedmath.h"

namespace ged
{
    struct Ray
    {
        vec3 origin;
        vec3 direction;

        Ray(const vec3& o = vec3(0), const vec3& d = vec3(0, 0, 1)) :
            origin(o), direction(d)
        {}
    };
}