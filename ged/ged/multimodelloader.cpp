#include "multimodelloader.h"

#include <fstream>
#include <sstream>
#include <ged/core/iohelper.h>
#include <ged/model.h>

namespace ged
{
    std::vector<Model*> LoadMultiModel(const GraphicsDevice& gfx_dev, const char* filename)
    {
        using std::getline;
        assert(filename);

        std::vector<Model*> models;

        std::ifstream file(filename, std::ifstream::in);
        if (!file.is_open())
        {
            printf("There was a problem opening the model file: '%s' \n", filename);
            return models;
        }

        std::string path = GetPath(filename);

        std::string line;
        std::vector<Material*> materials;
        std::map<std::string, size_t> mat_idx_map;
        std::map<std::string, std::string> grp_mat_map;

        Mesh* mesh = nullptr;

        while (file.good() && !file.eof()) {
            getline(file, line);
            if (line[0] == '#' || line[0] == '\n')
                continue;

            std::stringstream ss(line);
            std::string word;
            ss >> word;

            if (word == "MESH_FILE")
            {
                std::string mesh_file;
                ss >> mesh_file;
                mesh = LoadMesh(gfx_dev, (path + mesh_file).c_str());
            }
            else if (word == "MATERIAL_FILE")
            {
                std::string material_file;
                ss >> material_file;
                materials = LoadMaterials(gfx_dev, (path + material_file).c_str(), &mat_idx_map);
            }
            else if (word == "GROUP_MATERIAL")
            {
                std::string group;
                std::string material;

                ss >> group;
                ss >> material;

                grp_mat_map[group] = material;
            }
        }

        file.close();

        for (auto grp_mat_pair : grp_mat_map)
        {
            const std::string& grp_str = grp_mat_pair.first;
            const std::string& mat_str = grp_mat_pair.second;

            // Make sure the mapped group exists
            i32 grp_idx =  mesh->GetGroupIndex(SID(grp_str));
            if (grp_idx > -1)
            {
                Material* mat = nullptr;
                auto mat_idx = mat_idx_map.find(mat_str);
                if (mat_idx != mat_idx_map.end())
                    mat = materials[mat_idx->second];

                Model* mdl = new Model();
                mdl->SetMesh(mesh);
                mdl->MapMaterialToGroupIdx(mat, static_cast<size_t>(grp_idx));
                models.push_back(mdl);
            }
            else
                printf("group was not found \n");
        }

        return std::move(models);
    }
}