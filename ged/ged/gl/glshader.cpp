#include "glshader.h"

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include "../core/common.h"
#include "gl.h"
#include <ged/global.h>

static GLenum GLShaderTypeMap[] =
{
    GL_VERTEX_SHADER,
    GL_FRAGMENT_SHADER,
    GL_GEOMETRY_SHADER,
    GL_COMPUTE_SHADER,
    GL_TESS_CONTROL_SHADER,
    GL_TESS_EVALUATION_SHADER
};

bool ValidateShader(GLuint shader)
{
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE) {
        int infologLength = 0;
        int charsWritten = 0;
        char *infoLog;

        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infologLength);
        if (infologLength > 0) {
            infoLog = (char *)malloc(infologLength);
            glGetShaderInfoLog(shader, infologLength, &charsWritten, infoLog);
            printf("%s\n", infoLog);
            free(infoLog);
        }
        return false;
    }
    return true;
}

bool ValidateProgram(GLuint program)
{
    GLint status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);

    if (status != GL_TRUE) {
        int infologLength = 0;
        int charsWritten = 0;
        char *infoLog;

        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infologLength);
        if (infologLength > 0) {
            infoLog = (char *)malloc(infologLength);
            glGetProgramInfoLog(program, infologLength, &charsWritten, infoLog);
            printf("%s\n", infoLog);
            free(infoLog);
        }
        return false;
    }
    return true;
}

// Lighthouse OpenGL
char *TextFileRead(const char *fn) {
    FILE *fp;
    char *content = NULL;
    size_t count = 0;

    if (fn != NULL) {
#ifdef _MSC_VER
        fopen_s(&fp, fn, "rt");
#else
        fp = fopen(fn, "rt");
#endif

        if (fp != NULL) {
            fseek(fp, 0, SEEK_END);
            count = ftell(fp);
            rewind(fp);
            if (count > 0) {
                content = (char *)malloc(sizeof(char) * (count + 1));
                count = fread(content, sizeof(char), count, fp);
                content[count] = '\0';
            }
            fclose(fp);
        }
    }
    return content;
}

GLuint CreateShaderStage(GLenum type, const char* file)
{
    GLuint id = glCreateShader(type);

    char* src = TextFileRead(file);

    if (src == NULL) {
        printf("Shader (%s) could not be read. \n", file);
        return GL_INVALID_VALUE;
    }

    // Set shader source
    const char * c_src = src;
    glShaderSource(id, 1, &c_src, NULL);
    free(src);

    // Compile
    glCompileShader(id);
    if(!ValidateShader(id))
        return GL_INVALID_VALUE;

    return id;
}

namespace ged
{
    namespace gl
    {
        Shader::Shader(const std::initializer_list<ShaderStage>& list) :
            ged::Shader(list),
            m_program_id(-1)
        {
            m_program_id = glCreateProgram();

            for (const auto& item : list)
            {
                GLuint shad = CreateShaderStage(GLShaderTypeMap[static_cast<i32>(item.type)], item.filename.c_str());
                glAttachShader(m_program_id, shad);
            }

            // Link and validate
            glLinkProgram(m_program_id);
            if(!ValidateProgram(m_program_id))
            {
                exit(-1);
            }
        }

        Shader::~Shader()
        {
            glDeleteProgram(m_program_id);
        }

        void Shader::Activate() const
        {
            glUseProgram(m_program_id);
        }
    }
}
