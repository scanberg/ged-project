#include "glindex.h"
#include "../core/common.h"

/*

enum IndexType
{
    GED_TYPE_U8,
    GED_TYPE_U16,
    GED_TYPE_U32
};

enum IndexPrimitive
{
    GED_POINTS,
    GED_LINES,
    GED_LINES_ADJACENCY
    GED_LINE_STRIP,
    GED_LINE_STRIP_ADJACENCY,
    GED_LINE_LOOP,
    GED_TRIANGLES,
    GED_TRIANGLES_ADJACENCY,
    GED_TRIANGLE_STRIP,
    GED_TRIANGLE_STRIP_ADJACENCY,
    GED_TRIANGLE_FAN
};

*/

static const GLenum GLByteSizeTable[]
{
    1,
    2,
    4
};

static const GLenum GLIndexTypeTable[]
{
    GL_UNSIGNED_BYTE,
    GL_UNSIGNED_SHORT,
    GL_UNSIGNED_INT
};

static const GLenum GLIndexPrimitiveTable[]
{
    GL_POINTS,
    GL_LINES,
    GL_LINES_ADJACENCY,
    GL_LINE_STRIP,
    GL_LINE_STRIP_ADJACENCY,
    GL_LINE_LOOP,
    GL_TRIANGLES,
    GL_TRIANGLES_ADJACENCY,
    GL_TRIANGLE_STRIP,
    GL_TRIANGLE_STRIP_ADJACENCY,
    GL_TRIANGLE_FAN
};

namespace ged
{
    namespace gl
    {
        IndexFormat::IndexFormat(const IndexFormatDescriptor& desc) :
            type(GLIndexTypeTable[static_cast<i32>(desc.type)]),
            primitive(GLIndexPrimitiveTable[static_cast<i32>(desc.primitive)]),
            index_byte_size(GLByteSizeTable[static_cast<i32>(desc.type)])
        {}
    }
}