#pragma once
#include <vector>
#include <initializer_list>
#include <glm/gtc/packing.hpp>
#include "../core/common.h"
#include "../core/gedmath.h"
#include "../graphics/vertex.h"
#include "gl.h"

namespace ged
{
	namespace gl
	{
		class VertexArrayObject
		{
		public:
			VertexArrayObject()
			{
				glGenVertexArrays(1, &m_id);
			}

			~VertexArrayObject()
			{
				glDeleteVertexArrays(1, &m_id);
			}

			inline void Bind() const
			{
				glBindVertexArray(m_id);
			}

			inline void Unbind() const
			{
				glBindVertexArray(0);
			}

			inline GLuint GetID() const { return m_id; }
		private:
			GLuint m_id;
		};

		struct VertexComponent
		{
			VertexComponent(GLint size, GLenum type, GLboolean normalized, GLboolean integer) :
                size(size), type(type), normalized(normalized), integer(integer)
			{
				// Assert Size based upon Type
				assert( (type == GL_INT_2_10_10_10_REV || type == GL_UNSIGNED_INT_2_10_10_10_REV) ?
						(size == 4 || size == GL_BGRA) : (size >= 0 && size <= 4) );
				assert( (type == GL_UNSIGNED_INT_10F_11F_11F_REV) ? (size == 3) : true );

			}

			GLint size;
			GLenum type;
            GLuint byte_size;
            GLuint offset;
			GLboolean normalized;
            GLboolean integer;
		};

		class VertexFormat
        {
		public:
			VertexFormat(const VertexFormatDescriptor& vfd);

			size_t GetTotalByteSize() const;
			size_t GetNumComponents() const;
			const std::vector<VertexComponent>& GetComponents() const;

			void SetupVertexPointers() const;

		private:
			std::vector<VertexComponent> m_components;
			size_t m_total_byte_size;
		};

		inline size_t VertexFormat::GetTotalByteSize() const
		{
			return m_total_byte_size;
		}

		inline size_t VertexFormat::GetNumComponents() const
		{
			return m_components.size();
		}

		inline const std::vector<VertexComponent>& VertexFormat::GetComponents() const
		{
			return m_components;
		}
	}
}
