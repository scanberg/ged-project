#include "glmesh.h"

namespace ged
{
    namespace gl
    {
        Mesh::Mesh(const ged::VertexFormatDescriptor& vfd,
                   const ged::IndexFormatDescriptor& ifd,
                   const void* vertex_data,
                   size_t v_data_byte_size,
                   const void* index_data,
                   size_t i_data_byte_size,
                   const Group* group_data,
                   size_t group_count,
                   const AABB* aabb_data,
                   size_t aabb_count) :
            ged::Mesh(vfd, ifd),
            m_vertex_format(vfd),
            m_index_format(ifd)
        {
            m_vertex_array_object.Bind();
            m_vertex_buffer.Bind();
            m_vertex_format.SetupVertexPointers();
            m_vertex_array_object.Unbind();

            if (vertex_data != nullptr)
                SetVertexData(vertex_data, v_data_byte_size);
           
            if (index_data != nullptr)
                SetIndexData(index_data, i_data_byte_size);

            if (group_data != nullptr)
                GetGroups() = std::vector<Group>(group_data, group_data + group_count);
            else if (index_data)
                GetGroups().push_back(Mesh::Group(0, static_cast<u32>(m_index_count)));

            if (aabb_data != nullptr)
                GetGroupAABBs() = std::vector<AABB>(aabb_data, aabb_data + aabb_count);
        }

        void Mesh::SetVertexData(const void* data, size_t byte_size)
        {
            m_vertex_buffer.SetData(data, byte_size);
        }

        void* Mesh::MapVertexData()
        {
            void* data = m_vertex_buffer.MapData(GL_READ_WRITE);
            return data;
        }

        void Mesh::UnmapVertexData()
        {
            m_vertex_buffer.UnmapData();
        }

        void Mesh::SetIndexData(const void* data, size_t byte_size)
        {
            m_index_count = static_cast<GLuint>(byte_size / m_index_format.index_byte_size);
            printf("index count: %i \n", m_index_count);
            m_index_buffer.SetData(data, byte_size);
        }

        void* Mesh::MapIndexData()
        {
            assert(false);
            return nullptr;
        }

        void Mesh::UnmapIndexData()
        {
            return;
        }
    }
}