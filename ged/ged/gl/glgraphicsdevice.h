#pragma once

#include "gl.h"
#include "../core/input.h"
#include "../graphics/graphicsdevice.h"

namespace ged
{
    class Mesh;
    class Shader;
    class Texture;

    namespace gl
    {
        class GraphicsDevice : public ged::GraphicsDevice, public InputStateUpdater
        {
        public:
            void Init(const GraphicsConfig& cfg) final;
            void Shutdown() final;
            
            const ged::Mesh* GetQuad() const final;
            const ged::Mesh* GetCube() const final;
            const ged::Mesh* GetLineCube() const final;

            const ged::Texture* GetCheckerBoardTexture() const final;
            const ged::Texture* GetSolidWhiteTexture() const final;

            ged::Mesh* CreateMesh(const VertexFormatDescriptor& vfd,
                                         const IndexFormatDescriptor& ifd,
                                         const void* vertex_data,
                                         size_t vertex_data_byte_size,
                                         const void* index_data,
                                         size_t index_data_byte_size) const final;

            ged::Texture* CreateTexture(const TextureDescriptor& tex_desc, const void* data) const final;
            ged::Shader* CrateShader(const std::initializer_list<ShaderStage>& list) const final;

            void ClearBuffers(ClearBufferTarget target) const final;
            void FlushCommandQueue() const final;
            void SwapBuffers() const final;

            void UpdateInputState(InputState* state) const final;

            GLFWwindow* GetGLFWwindow()
            {
                return m_window;
            }

        private:
            GLFWwindow* m_window;
        };
    }
}