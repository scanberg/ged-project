#pragma once
#include "../graphics/index.h"
#include "gl.h"

namespace ged
{
    namespace gl
    {
        class IndexFormat
        {
        public:
            IndexFormat(const IndexFormatDescriptor& desc);

            const GLenum type;
            const GLenum primitive;
            const GLuint index_byte_size;
        };
    }
}