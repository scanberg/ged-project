#pragma once

#include "gl.h"

namespace ged
{
	namespace gl
	{
        class Buffer
        {
        public:
            GLuint GetID() const { return m_id; }

            void Bind() const
            {
                glBindBuffer(m_target, m_id);
            }

            void Unbind() const
            {
                glBindBuffer(m_target, 0);
            }

            void SetData(const void* data, size_t byte_size, GLenum usage = GL_STATIC_DRAW)
            {
                assert(usage == GL_STATIC_DRAW || usage == GL_STATIC_READ || usage == GL_STATIC_COPY ||
                       usage == GL_DYNAMIC_DRAW || usage == GL_DYNAMIC_READ || usage == GL_DYNAMIC_COPY ||
                       usage == GL_STREAM_DRAW || usage == GL_STREAM_READ || usage == GL_STREAM_COPY);
                Bind();
                glBufferData(m_target, byte_size, data, usage);
                Unbind();
            }

            void* MapData(GLenum access)
            {
                assert(access == GL_READ_WRITE ||
                       access == GL_READ_ONLY ||
                       access == GL_WRITE_ONLY);
                Bind();
                return glMapBuffer(m_target, access);
            }

            void UnmapData()
            {
                glUnmapBuffer(m_target);
                Unbind();
            }
        protected:
            Buffer(GLenum target) :
                m_target(target)
            {
                glGenBuffers(1, &m_id);
            }
            ~Buffer()
            {
                glDeleteBuffers(1, &m_id);
            }

            GLenum m_target;
            GLuint m_id;
        };

		class VertexBuffer : public Buffer
		{
        public:
            VertexBuffer() : Buffer(GL_ARRAY_BUFFER) {}
		};

		class VertexBufferRange
		{
		public:
            VertexBufferRange(VertexBuffer* vb, u32 offset, u32 length) :
                m_buffer(vb), m_offset(offset), m_length(length) {}

            VertexBuffer* GetVertexBuffer()
            {
                return m_buffer;
            }

            u32 GetOffset() const
            {
                return m_offset;
            }

            u32 GetLength() const
            {
                return m_length;
            }
		private:
			VertexBuffer* m_buffer;
			u32 m_offset;
			u32 m_length;
		};

		class IndexBuffer : public Buffer
		{
		public:
            IndexBuffer() : Buffer(GL_ELEMENT_ARRAY_BUFFER) {}
		};

		class IndexBufferRange
		{
		public:
            IndexBufferRange(IndexBuffer* ib, u32 offset, u32 length) :
                m_buffer(ib), m_offset(offset), m_length(length) {}

            IndexBuffer* GetIndexBuffer()
            {
                return m_buffer;
            }

            u32 GetOffset() const
            {
                return m_offset;
            }

            u32 GetLength() const
            {
                return m_length;
            }
		private:
			IndexBuffer* m_buffer;
			u32 m_offset;
			u32 m_length;
		};
	}
}