#include "glbasicrenderer.h"

#include <algorithm>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../graphics/texture.h"
#include "../model.h"
#include "../material.h"
#include "gl.h"
#include "glshader.h"
#include "glmesh.h"
#include "glgraphicsdevice.h"
#include "gltexture.h"

#include <ged/global.h>

// Define DRAW_AABB if you want to see aabbs of GameObjects 
//#define DRAW_AABB

struct RenderJob
{
    const ged::gl::Shader* shader;
    const ged::Material* mat;
    const ged::gl::Mesh* mesh;

    size_t matrix_index;
    GLuint offset;
    GLuint length;

    inline bool operator < (const RenderJob& other) const
    {
        return (shader < other.shader) ||
            ((shader == other.shader) && (mat < other.mat));
    }
};

static std::vector<ged::mat4> matrices;
static std::vector<RenderJob> renderlist;

namespace ged
{
    namespace gl
    {
        BasicRenderer::BasicRenderer() :
            m_gfx_dev(nullptr),
            m_diffuse_shader(nullptr),
            m_diffuse_mask_shader(nullptr),
            m_skinned_diffuse_shader(nullptr),
            m_skinned_diffuse_mask_shader(nullptr),
            m_default_material(nullptr),
            m_solid_white_material(nullptr)
        {}

        void BasicRenderer::Init(const ged::GraphicsDevice& gfx_dev)
        {
            // Make sure
            m_gfx_dev = dynamic_cast<const gl::GraphicsDevice*>(&gfx_dev);
            assert(m_gfx_dev != nullptr);

            m_diffuse_shader = new Shader({
                ShaderStage(ShaderStageType::GED_VERTEX_SHADER, global::RESOURCE_BASE_PATH + "shaders/basic.vert"),
                ShaderStage(ShaderStageType::GED_FRAGMENT_SHADER, global::RESOURCE_BASE_PATH + "shaders/basic.frag"),
            });

            m_diffuse_mask_shader = new Shader({
                ShaderStage(ShaderStageType::GED_VERTEX_SHADER, global::RESOURCE_BASE_PATH + "shaders/basic.vert"),
                ShaderStage(ShaderStageType::GED_FRAGMENT_SHADER, global::RESOURCE_BASE_PATH + "shaders/basic_mask.frag"),
            });

            const Texture2D* tex_checker_board =
                reinterpret_cast<const Texture2D*>(m_gfx_dev->GetCheckerBoardTexture());
            glBindTexture(GL_TEXTURE_2D, tex_checker_board->GetID());
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glBindTexture(GL_TEXTURE_2D, 0);

            m_default_material = new Material();
            m_default_material->SetComponent(
                PBRMaterialComponent::GED_BASE_COLOR,
                Material::Component(tex_checker_board));

            const Texture2D* tex_solid_white =
                reinterpret_cast<const Texture2D*>(m_gfx_dev->GetSolidWhiteTexture());
            glBindTexture(GL_TEXTURE_2D, tex_solid_white->GetID());
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glBindTexture(GL_TEXTURE_2D, 0);

            m_solid_white_material = new Material();
            m_solid_white_material->SetComponent(
                PBRMaterialComponent::GED_BASE_COLOR,
                Material::Component(tex_solid_white));
        }

        void BasicRenderer::Shutdown()
        {
            delete m_diffuse_shader;
            delete m_diffuse_mask_shader;
            delete m_skinned_diffuse_shader;
            delete m_skinned_diffuse_mask_shader;
            delete m_default_material;
            delete m_solid_white_material;
        }

        void BasicRenderer::Render(const std::vector<GameObject>& objects,
                                   const CameraParameters& param)
        {
            assert(m_gfx_dev != nullptr);

            glEnable(GL_DEPTH_TEST);
            glEnable(GL_CULL_FACE);
            glCullFace(GL_BACK);

            renderlist.clear();
            renderlist.reserve(objects.size());

            matrices.clear();
            matrices.reserve(objects.size());

            // Insert default matrix
            matrices.push_back(param.world_to_view_matrix);

            for (size_t i = 0; i < objects.size(); i++)
            {
                GameObject go = objects[i];
                const TransformComponent* tc = go.GetComponent<TransformComponent>();
                const RenderComponent* rc = go.GetComponent<RenderComponent>();

                if (rc && rc->visible && rc->model && rc->model->GetMesh()) {
                    size_t matrix_index = 0;
                    mat4 model_to_view_mat;

                    if (tc != nullptr)
                    {
                        const mat4& model_to_world = tc->transform.Matrix();
                        matrix_index = matrices.size();
                        model_to_view_mat = param.world_to_view_matrix * model_to_world;
                        matrices.push_back(model_to_view_mat);
                    }
                        
                    const Mesh* mesh = reinterpret_cast<const gl::Mesh*>(rc->model->GetMesh());
                    
                    const std::vector<Mesh::Group>& groups = mesh->GetGroups();
                    const std::vector<GroupMaterialMapping>& grp_mat_mappings = rc->model->GetGroupMaterialMappings();
                    //const std::vector<bool>& group_flags = rc->model->GetGroupFlags();

                    for (const auto& grp_mat_map : grp_mat_mappings)
                    {
                        size_t grp_idx = grp_mat_map.group_idx;

                        RenderJob rj {
                            nullptr, grp_mat_map.material, mesh, matrix_index,
                            groups[grp_idx].offset, groups[grp_idx].length };

                        // If the material is not set, use default material
                        if (rj.mat == nullptr)
                            rj.mat = m_default_material;

                        // If the material has the alpha texture set, use the masked shader
                        if (rj.mat->GetComponent(PBRMaterialComponent::GED_ALPHA).texture)
                            rj.shader = m_diffuse_mask_shader;
                        else
                            rj.shader = m_diffuse_shader;

                        // Add the render job
                        renderlist.push_back(rj);
                    }
#ifdef DRAW_AABB
                    {
                        const Mesh* line_cube = reinterpret_cast<const Mesh*>(m_gfx_dev->GetLineCube());
                        const mat4 aabb_scale_mat = glm::scale(mat4(), rc->model->GetAABB().size());
                        const mat4 aabb_offset_mat = glm::translate(mat4(), rc->model->GetAABB().min_box);

                        size_t matrix_index = matrices.size();
                        matrices.push_back(model_to_view_mat * aabb_offset_mat * aabb_scale_mat);

                        RenderJob rj{
                            m_diffuse_shader, m_solid_white_material, line_cube, matrix_index,
                            0, line_cube->GetIndexCount() };

                        renderlist.push_back(rj);
                    }
#endif
                }
            }

            std::sort(renderlist.begin(), renderlist.end());

            const Shader* current_shader = nullptr;
            const Material* current_mat = nullptr;
            const Mesh* current_mesh = nullptr;
            size_t current_matrix_index = -1;

            GLint proj_loc = -1;
            GLint view_loc = -1;

            const GLuint* base_ptr = 0;
            bool update_matrix_representation = false;

            GLenum primitive = GL_TRIANGLES;
            GLenum type = GL_UNSIGNED_INT;
            
            for (const auto& job : renderlist)
            {
                if (current_shader != job.shader)
                {
                    current_shader = job.shader;
                    current_shader->Activate();
                    GLuint id = current_shader->GetID();
                    
                    proj_loc = glGetUniformLocation(id, "proj_matrix");
                    view_loc = glGetUniformLocation(id, "view_matrix");
                    
                    for (int i = 0; i < 4; i++) {
                        std::string tex_str = std::string("texture") + std::to_string(i);
                        GLint tex_loc = glGetUniformLocation(id, tex_str.c_str());
                        if (tex_loc > -1)
                            glUniform1i(tex_loc, i);
                    }
                    
                    glUniformMatrix4fv(proj_loc, 1, GL_FALSE, glm::value_ptr(param.proj_matrix));
                    update_matrix_representation = true;
                }
                
                if (current_matrix_index != job.matrix_index) {
                    current_matrix_index = job.matrix_index;
                    update_matrix_representation = true;
                }

                if (update_matrix_representation) {
                    update_matrix_representation = false;
                    glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(matrices[current_matrix_index]));
                }

                if (current_mesh!= job.mesh) {
                    current_mesh = job.mesh;
                    current_mesh->GetVertexArrayObject().Bind();
                    current_mesh->GetIndexBuffer().Bind();
                    
                    type = current_mesh->GetIndexFormat().type;
                    primitive = current_mesh->GetIndexFormat().primitive;
                }

                if (current_mat != job.mat) {
                    current_mat = job.mat;

                    // Check the color channel separately
                    if (current_mat->GetComponent(0).texture == nullptr)
                        m_gfx_dev->GetCheckerBoardTexture()->BindToSlot(0);
                    else
                        current_mat->GetComponent(0).texture->BindToSlot(0);

                    // Apply the rest
                    for (u32 i = 1; i < 8; i++)
                    {
                        if (current_mat->GetComponent(i).texture)
                            current_mat->GetComponent(i).texture->BindToSlot(i);
                    }
                }

                glDrawElements(primitive, job.length, type, base_ptr + job.offset);
            }

            if (current_mesh) {
                current_mesh->GetIndexBuffer().Unbind();
                current_mesh->GetVertexArrayObject().Unbind();
            }
        }
    }
}
