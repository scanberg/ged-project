#pragma once

#include <vector>
#include "../core/common.h"
#include "../graphics/renderer.h"
#include "../camera.h"
#include "glgraphicsdevice.h"

namespace ged
{
    class Material;

    namespace gl
    {
        class Shader;

        class BasicRenderer : public ged::Renderer
        {
        public:
            BasicRenderer();

            virtual void Init(const ged::GraphicsDevice& gfx_dev) override;
            virtual void Shutdown() override;
            
            virtual void Render(const std::vector<GameObject>& objects,
                                const CameraParameters& param) override;

        private:
            const gl::GraphicsDevice* m_gfx_dev;

            Shader* m_diffuse_shader;
            Shader* m_diffuse_mask_shader;
            Shader* m_skinned_diffuse_shader;
            Shader* m_skinned_diffuse_mask_shader;

            Material* m_default_material;
            Material* m_solid_white_material;
        };
    }
}