#pragma once
#include "../graphics/texture.h"
#include "gl.h"

namespace ged
{
	namespace gl
	{
		class Texture : public ged::Texture
		{
		public:
            virtual void BindToSlot(u32 slot) const override;

			inline GLuint GetID() const
			{
				return m_id;
			}
		protected:
            Texture(const TextureDescriptor& tex_desc, GLenum target);
            ~Texture();

			GLuint m_id;
            GLenum m_target;
		};

		class Texture1D : public Texture
		{
		public:
            Texture1D(const TextureDescriptor& tex_desc, const void* data = nullptr);

            virtual void SetData(const void* data, int level) override;
            virtual void GetData(void* data, int level) override;
		};

		class Texture2D : public Texture
		{
		public:
            Texture2D(const TextureDescriptor& tex_desc, const void* data = nullptr);

            virtual void SetData(const void* data, int level) override;
            virtual void GetData(void* data, int level) override;
		};

		class Texture3D : public Texture
		{
		public:
            Texture3D(const TextureDescriptor& tex_desc, const void* data = nullptr);

            virtual void SetData(const void* data, int level) override;
            virtual void GetData(void* data, int level) override;
		};
	}
}