#pragma once

#include <cassert>
#include <initializer_list>
#include "../graphics/shader.h"
#include "gl.h"

namespace ged
{
    namespace gl
    {
        class Shader : public ged::Shader
        {
        public:
            Shader(const std::initializer_list<ShaderStage>& list);
            virtual ~Shader();

            virtual void Activate() const override;
            GLuint GetID() const { return m_program_id; }

        private:
            GLuint m_program_id;
        };
    }
}