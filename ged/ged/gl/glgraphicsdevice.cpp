#include <cstdlib>
#include <iostream>
#include <cstdio>
#include "glgraphicsdevice.h"
#include "gl.h"
#include "glmesh.h"
#include "gltexture.h"
#include "glshader.h"

static void APIENTRY OpenglCallbackFunction(
    GLenum,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei,
    const GLchar* message,
    const void*){

    std::cout << "---------------------opengl-callback-start------------" << std::endl;
    std::cout << "message: " << message << std::endl;
    std::cout << "type: ";
    switch (type) {
    case GL_DEBUG_TYPE_ERROR:
        std::cout << "ERROR";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        std::cout << "DEPRECATED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        std::cout << "UNDEFINED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        std::cout << "PORTABILITY";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        std::cout << "PERFORMANCE";
        break;
    case GL_DEBUG_TYPE_OTHER:
        std::cout << "OTHER";
        break;
    }
    std::cout << std::endl;

    std::cout << "id: " << id << std::endl;
    std::cout << "severity: ";
    switch (severity){
    case GL_DEBUG_SEVERITY_LOW:
        std::cout << "LOW";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
        std::cout << "MEDIUM";
        break;
    case GL_DEBUG_SEVERITY_HIGH:
        std::cout << "HIGH";
        break;
    }
    std::cout << std::endl;
    std::cout << "---------------------opengl-callback-end--------------" << std::endl;
}

void GLFWCursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
    ged::InputState* state = reinterpret_cast<ged::InputState*>(glfwGetWindowUserPointer(window));
    if (state != nullptr)
        state->mouse_pos = ged::vec2(static_cast<ged::f32>(xpos), static_cast<ged::f32>(ypos));
}

void GLFWMouseButtonCallback(GLFWwindow* window, int button, int action, int)
{
    ged::InputState* state = reinterpret_cast<ged::InputState*>(glfwGetWindowUserPointer(window));
    if (state != nullptr)
        state->mouse[button] = (action != GLFW_RELEASE);
}

void GLFWScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    ged::InputState* state = reinterpret_cast<ged::InputState*>(glfwGetWindowUserPointer(window));
    if (state != nullptr)
        state->mouse_scroll = ged::vec2(xoffset, yoffset);
}

void GLFWKeyCallback(GLFWwindow* window, int key, int, int action, int)
{
    ged::InputState* state = reinterpret_cast<ged::InputState*>(glfwGetWindowUserPointer(window));
    if (state != nullptr)
        state->keyboard[key] = (action != GLFW_RELEASE);
}

namespace ged
{
    namespace gl
    {
        void GraphicsDevice::Init(const GraphicsConfig& cfg)
        {
            /* Initialize the library */
            if (!glfwInit()) {
                printf("Failed to initialize GLFW\n");
                exit(-1);
            }

            // Set up hints for context
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

            // Create a windowed mode window and its OpenGL context
            m_window = glfwCreateWindow(cfg.resolution_x, cfg.resolution_y, cfg.title.c_str(), nullptr, nullptr);
            if (!m_window) {
                printf("Failed to create window\n");
                glfwTerminate();
                exit(-1);
            }

            // Make the window's context current
            glfwMakeContextCurrent(m_window);
            gladLoadGL();

            glfwSwapInterval(cfg.vertical_sync);
            glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

            // Set callbacks
            glfwSetCursorPosCallback(m_window, GLFWCursorPosCallback);
            glfwSetMouseButtonCallback(m_window, GLFWMouseButtonCallback);
            glfwSetScrollCallback(m_window, GLFWScrollCallback);
            glfwSetKeyCallback(m_window, GLFWKeyCallback);

            glfwSetInputMode(m_window, GLFW_STICKY_KEYS, 1);

#ifndef NDEBUG
            // If we are using debug mode, enable OpenGL debug callback
            if (glDebugMessageCallback){
                std::cout << "Register OpenGL debug callback " << std::endl;
                glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
                glDebugMessageCallback(OpenglCallbackFunction, nullptr);
                GLuint unusedIds = 0;
                glDebugMessageControl(GL_DONT_CARE,
                    GL_DONT_CARE,
                    GL_DONT_CARE,
                    0,
                    &unusedIds,
                    true);
            }
            else
                std::cout << "glDebugMessageCallback not available" << std::endl;
#endif
        }

        void GraphicsDevice::Shutdown()
        {
            glfwDestroyWindow(m_window);
            glfwTerminate();
        }

        const ged::Mesh* GraphicsDevice::GetQuad() const
        {
            static const float vertex_data[] =
            {
                /*
                X    Y    Z    NX   NY   NZ   TCX  TCY
                */
                0.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f,
                1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 1.f, 0.f,
                1.f, 1.f, 0.f, 0.f, 0.f, 1.f, 1.f, 1.f,
                0.f, 1.f, 0.f, 0.f, 0.f, 1.f, 0.f, 1.f
            };

            static const u8 index_data[] =
            {
                0, 1, 2,
                2, 3, 0
            };

            static const Mesh* quad = new Mesh(VertexDefault::format,
                                               IndexFormatDescriptor(IndexType::GED_TYPE_U8, IndexPrimitive::GED_TRIANGLES),
                                               vertex_data, 32 * sizeof(float),
                                               index_data, 6 * sizeof(u8));

            return quad;
        }

        const ged::Mesh* GraphicsDevice::GetCube() const
        {
            // Offset
            static const vec3 O(0.0f);

            static const float vertex_data[] =
            {
                /*
                X           Y           Z            NX    NY    NZ    TCX  TCY
                */
                O[0] + 0.f, O[1] + 0.f, O[2] + 0.f, -1.f,  0.f,  0.f,  0.f, 0.f,
                O[0] + 0.f, O[1] + 0.f, O[2] + 0.f,  0.f, -1.f,  0.f,  0.f, 0.f,
                O[0] + 0.f, O[1] + 0.f, O[2] + 0.f,  0.f,  0.f, -1.f,  1.f, 0.f,

                O[0] + 1.f, O[1] + 0.f, O[2] + 0.f,  1.f,  0.f,  0.f,  1.f, 0.f,
                O[0] + 1.f, O[1] + 0.f, O[2] + 0.f,  0.f, -1.f,  0.f,  1.f, 0.f,
                O[0] + 1.f, O[1] + 0.f, O[2] + 0.f,  0.f,  0.f, -1.f,  0.f, 0.f,

                O[0] + 1.f, O[1] + 1.f, O[2] + 0.f,  1.f,  0.f,  0.f,  1.f, 1.f,
                O[0] + 1.f, O[1] + 1.f, O[2] + 0.f,  0.f,  1.f,  0.f,  1.f, 1.f,
                O[0] + 1.f, O[1] + 1.f, O[2] + 0.f,  0.f,  0.f, -1.f,  0.f, 1.f,

                O[0] + 0.f, O[1] + 1.f, O[2] + 0.f, -1.f,  0.f,  0.f,  0.f, 1.f,
                O[0] + 0.f, O[1] + 1.f, O[2] + 0.f,  0.f,  1.f,  0.f,  0.f, 1.f,
                O[0] + 0.f, O[1] + 1.f, O[2] + 0.f,  0.f,  0.f, -1.f,  1.f, 1.f,

                O[0] + 0.f, O[1] + 0.f, O[2] + 1.f, -1.f,  0.f,  0.f,  1.f, 0.f,
                O[0] + 0.f, O[1] + 0.f, O[2] + 1.f,  0.f, -1.f,  0.f,  0.f, 1.f,
                O[0] + 0.f, O[1] + 0.f, O[2] + 1.f,  0.f,  0.f,  1.f,  0.f, 0.f,

                O[0] + 1.f, O[1] + 0.f, O[2] + 1.f,  1.f,  0.f,  0.f,  0.f, 0.f,
                O[0] + 1.f, O[1] + 0.f, O[2] + 1.f,  0.f, -1.f,  0.f,  1.f, 1.f,
                O[0] + 1.f, O[1] + 0.f, O[2] + 1.f,  0.f,  0.f,  1.f,  1.f, 0.f,

                O[0] + 1.f, O[1] + 1.f, O[2] + 1.f,  1.f,  0.f,  0.f,  0.f, 1.f,
                O[0] + 1.f, O[1] + 1.f, O[2] + 1.f,  0.f,  1.f,  0.f,  1.f, 0.f,
                O[0] + 1.f, O[1] + 1.f, O[2] + 1.f,  0.f,  0.f,  1.f,  1.f, 1.f,

                O[0] + 0.f, O[1] + 1.f, O[2] + 1.f, -1.f,  0.f,  0.f,  1.f, 1.f,
                O[0] + 0.f, O[1] + 1.f, O[2] + 1.f,  0.f,  1.f,  0.f,  0.f, 0.f,
                O[0] + 0.f, O[1] + 1.f, O[2] + 1.f,  0.f,  0.f,  1.f,  0.f, 1.f,
            };

            #define NX(i) (3 * i + 0)
            #define NY(i) (3 * i + 1)
            #define NZ(i) (3 * i + 2)

            static const u8 index_data[] =
            {
                NZ(0), NZ(3), NZ(2),
                NZ(0), NZ(2), NZ(1),
                
                NX(1), NX(2), NX(6),
                NX(1), NX(6), NX(5),
                
                NY(6), NY(2), NY(3),
                NY(6), NY(3), NY(7),

                NZ(6), NZ(7), NZ(4),
                NZ(6), NZ(4), NZ(5),

                NX(4), NX(7), NX(3),
                NX(4), NX(3), NX(0),

                NY(4), NY(0), NY(1),
                NY(4), NY(1), NY(5)
            };

            #undef NX
            #undef NY
            #undef NZ

            static const ged::Mesh::Group group_data[] =
            {
                { 0, 24 }
            };

            static const ged::AABB aabb_data[] =
            {
                { vec3(0), vec3(1) }
            };

            static const Mesh* cube = new Mesh(
                VertexDefault::format,
                IndexFormatDescriptor(IndexType::GED_TYPE_U8, IndexPrimitive::GED_TRIANGLES),
                vertex_data, 192 * sizeof(float),
                index_data, 36 * sizeof(u8),
                group_data, 1,
                aabb_data, 1);

            return cube;
        }

        const ged::Mesh* GraphicsDevice::GetLineCube() const
        {
            // Offset
            static const vec3 O(0.0f);

            static const float vertex_data[] =
            {
                /*
                X           Y           Z
                */
                O[0] + 0.f, O[1] + 0.f, O[2] + 0.f,
                O[0] + 1.f, O[1] + 0.f, O[2] + 0.f,
                O[0] + 1.f, O[1] + 1.f, O[2] + 0.f,
                O[0] + 0.f, O[1] + 1.f, O[2] + 0.f,

                O[0] + 0.f, O[1] + 0.f, O[2] + 1.f,
                O[0] + 1.f, O[1] + 0.f, O[2] + 1.f,
                O[0] + 1.f, O[1] + 1.f, O[2] + 1.f,
                O[0] + 0.f, O[1] + 1.f, O[2] + 1.f,
            };

            static const u8 index_data[] =
            {
                0, 1,
                1, 2,
                2, 3,
                0, 3,

                4, 5,
                5, 6,
                6, 7,
                4, 7,

                0, 4,
                1, 5,
                2, 6,
                3, 7
            };

            static const ged::Mesh::Group group_data[] =
            {
                { 0, 24 }
            };

            static const ged::AABB aabb_data[] =
            {
                { vec3(0), vec3(1) }
            };

            static const Mesh* cube = new Mesh(
                VertexFormatDescriptor({VertexComponentFormat::GED_FORMAT_R32G32B32_FLOAT}),
                IndexFormatDescriptor(IndexType::GED_TYPE_U8, IndexPrimitive::GED_LINES),
                vertex_data, 24 * sizeof(float),
                index_data, 24 * sizeof(u8),
                group_data, 1,
                aabb_data, 1);

            return cube;
        }

        const ged::Texture* GraphicsDevice::GetCheckerBoardTexture() const
        {
            static const u8 data[] =
            {
                255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0,
                0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255,
                255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0,
                0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255,
                255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0,
                0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255,
                255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0,
                0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255
            };

            static const TextureDescriptor desc(TextureType::GED_TEXTURE_2D, TextureFormat::GED_FORMAT_RGBA8, 8, 8, 1, 4);
            static Texture* tex = new Texture2D(desc, data);

            return tex;
        }

        const ged::Texture* GraphicsDevice::GetSolidWhiteTexture() const
        {
            static const u8 data[] =
            {
                255, 255, 255, 255
            };

            static const TextureDescriptor desc(TextureType::GED_TEXTURE_2D, TextureFormat::GED_FORMAT_RGBA8, 1, 1, 1, 1);
            static Texture* tex = new Texture2D(desc, data);

            return tex;
        }
        
        ged::Mesh* GraphicsDevice::CreateMesh(const VertexFormatDescriptor& vfd,
                                             const IndexFormatDescriptor& ifd,
                                             const void* vertex_data,
                                             size_t vertex_data_byte_size,
                                             const void* index_data,
                                             size_t index_data_byte_size) const
        {
            Mesh* mesh = new Mesh(vfd,
                                  ifd,
                                  vertex_data,
                                  vertex_data_byte_size,
                                  index_data,
                                  index_data_byte_size);
            return mesh;
        }

        ged::Texture* GraphicsDevice::CreateTexture(const TextureDescriptor& tex_desc, const void* data) const
        {
            Texture* tex = nullptr;

            if (tex_desc.type == TextureType::GED_TEXTURE_1D)
                tex = new Texture1D(tex_desc, data);
            else if (tex_desc.type == TextureType::GED_TEXTURE_2D)
                tex = new Texture2D(tex_desc, data);
            else if (tex_desc.type == TextureType::GED_TEXTURE_3D)
                tex = new Texture3D(tex_desc, data);
            else
            {
                // LOG ERROR
                assert(false);
            }
            
            return tex;
        }

        ged::Shader* GraphicsDevice::CrateShader(const std::initializer_list<ShaderStage>& list) const
        {
            Shader* shader = new Shader(list);
            return shader;
        }

        void GraphicsDevice::ClearBuffers(ClearBufferTarget target) const
        {
            static const GLenum GLClearBufferBits[]
            {
                GL_COLOR_BUFFER_BIT,
                GL_DEPTH_BUFFER_BIT,
                GL_STENCIL_BUFFER_BIT,
                GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT
            };

            glClear(GLClearBufferBits[target]);
        }

        void GraphicsDevice::FlushCommandQueue() const
        {
            glFlush();
        }

        void GraphicsDevice::SwapBuffers() const
        {
            glfwSwapBuffers(m_window);
        }

        void GraphicsDevice::UpdateInputState(InputState* state) const
        {
            glfwSetWindowUserPointer(m_window, reinterpret_cast<void*>(state));
            glfwPollEvents();
            glfwSetWindowUserPointer(m_window, nullptr);
        }
    }
}