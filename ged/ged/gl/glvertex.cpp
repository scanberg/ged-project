#include <cstdio>
#include <cctype>
#include "glvertex.h"

/*
GED_FORMAT_R32G32B32A32_FLOAT,
GED_FORMAT_R32G32B32A32_INT,
GED_FORMAT_R32G32B32A32_UINT,

GED_FORMAT_R32G32B32_FLOAT,
GED_FORMAT_R32G32B32_INT,
GED_FORMAT_R32G32B32_UINT,

GED_FORMAT_R32G32_FLOAT,
GED_FORMAT_R32G32_INT,
GED_FORMAT_R32G32_UINT,

GED_FORMAT_R32_FLOAT,
GED_FORMAT_R32_INT,
GED_FORMAT_R32_UINT,

GED_FORMAT_R16G16B16A16_FLOAT,
GED_FORMAT_R16G16B16A16_INT,
GED_FORMAT_R16G16B16A16_UINT,
GED_FORMAT_R16G16B16A16_INORM,
GED_FORMAT_R16G16B16A16_UNORM,

GED_FORMAT_R16G16_FLOAT,
GED_FORMAT_R16G16_INT,
GED_FORMAT_R16G16_UINT,
GED_FORMAT_R16G16_INORM,
GED_FORMAT_R16G16_UNORM,

GED_FORMAT_R8G8B8A8_INT,
GED_FORMAT_R8G8B8A8_UINT,
GED_FORMAT_R8G8B8A8_INORM,
GED_FORMAT_R8G8B8A8_UNORM,

GED_FORMAT_R8G8_INT,
GED_FORMAT_R8G8_UINT,
GED_FORMAT_R8G8_INORM,
GED_FORMAT_R8G8_UNORM,

GED_FORMAT_R10G10B10A2_INT,
GED_FORMAT_R10G10B10A2_UINT,
GED_FORMAT_R10G10B10A2_INORM,
GED_FORMAT_R10G10B10A2_UNORM,
GED_FORMAT_R11G11B10_FLOAT
*/

static const ged::gl::VertexComponent VertexComponentTable[]
{
    ged::gl::VertexComponent(4, GL_FLOAT, GL_FALSE, GL_FALSE),
    ged::gl::VertexComponent(4, GL_INT, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(4, GL_UNSIGNED_INT, GL_FALSE, GL_TRUE),

    ged::gl::VertexComponent(3, GL_FLOAT, GL_FALSE, GL_FALSE),
    ged::gl::VertexComponent(3, GL_INT, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(3, GL_UNSIGNED_INT, GL_FALSE, GL_TRUE),

    ged::gl::VertexComponent(2, GL_FLOAT, GL_FALSE, GL_FALSE),
    ged::gl::VertexComponent(2, GL_INT, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(2, GL_UNSIGNED_INT, GL_FALSE, GL_TRUE),

    ged::gl::VertexComponent(1, GL_FLOAT, GL_FALSE, GL_FALSE),
    ged::gl::VertexComponent(1, GL_INT, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(1, GL_UNSIGNED_INT, GL_FALSE, GL_TRUE),

    ged::gl::VertexComponent(4, GL_HALF_FLOAT, GL_FALSE, GL_FALSE),
    ged::gl::VertexComponent(4, GL_SHORT, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(4, GL_UNSIGNED_SHORT, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(4, GL_SHORT, GL_TRUE, GL_FALSE),
    ged::gl::VertexComponent(4, GL_UNSIGNED_SHORT, GL_TRUE, GL_FALSE),

    ged::gl::VertexComponent(2, GL_HALF_FLOAT, GL_FALSE, GL_FALSE),
    ged::gl::VertexComponent(2, GL_SHORT, GL_FALSE, GL_TRUE), 
    ged::gl::VertexComponent(2, GL_UNSIGNED_SHORT, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(2, GL_SHORT, GL_TRUE, GL_FALSE),
    ged::gl::VertexComponent(2, GL_UNSIGNED_SHORT, GL_TRUE, GL_FALSE),

    ged::gl::VertexComponent(4, GL_BYTE, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(4, GL_UNSIGNED_BYTE, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(4, GL_BYTE, GL_TRUE, GL_FALSE),
    ged::gl::VertexComponent(4, GL_UNSIGNED_BYTE, GL_TRUE, GL_FALSE),

    ged::gl::VertexComponent(2, GL_BYTE, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(2, GL_UNSIGNED_BYTE, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(2, GL_BYTE, GL_TRUE, GL_FALSE),
    ged::gl::VertexComponent(2, GL_UNSIGNED_BYTE, GL_TRUE, GL_FALSE),

    ged::gl::VertexComponent(4, GL_INT_2_10_10_10_REV, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(4, GL_UNSIGNED_INT_2_10_10_10_REV, GL_FALSE, GL_TRUE),
    ged::gl::VertexComponent(4, GL_INT_2_10_10_10_REV, GL_TRUE, GL_FALSE),
    ged::gl::VertexComponent(4, GL_UNSIGNED_INT_2_10_10_10_REV, GL_TRUE, GL_FALSE),

    ged::gl::VertexComponent(3, GL_UNSIGNED_INT_10F_11F_11F_REV, GL_FALSE, GL_FALSE)
};

static const GLsizei ByteSizeTable[]
{
    16, 16, 16,
    12, 12, 12,
    8, 8, 8,
    4, 4, 4,
    8, 8, 8, 8, 8,
    4, 4, 4, 4, 4,
    4, 4, 4, 4,
    2, 2, 2, 2,
    4, 4, 4, 4,
    4
};

static_assert(sizeof(VertexComponentTable) / sizeof(ged::gl::VertexComponent) ==
    static_cast<size_t>(ged::VertexComponentFormat::GED_NUM_VERTEX_COMPONENT_FORMATS),
    "GL Vertex Format table size mismatch");

static_assert(sizeof(ByteSizeTable) / sizeof(GLsizei) ==
    static_cast<size_t>(ged::VertexComponentFormat::GED_NUM_VERTEX_COMPONENT_FORMATS),
    "GL Vertex Format table size mismatch");

/*
	struct VertexComponent
	{
		VertexComponent(GLint size, GLenum type, GLboolean normalized) :
			size(size), type(type), normalized(normalized) {}

		GLint size;
		GLenum type;
		GLboolean normalized;

		GLuint total_byte_size;
		GLuint offset;
	};
*/

namespace ged
{
	namespace gl
	{
		VertexFormat::VertexFormat(const VertexFormatDescriptor& vfd)
		{
			m_total_byte_size = 0;
            m_components.reserve(vfd.components.size());

			for (const auto& component : vfd.components)
			{
                VertexComponent vc = VertexComponentTable[static_cast<i32>(component)];
                vc.byte_size = ByteSizeTable[static_cast<i32>(component)];
                vc.offset = static_cast<GLuint>(m_total_byte_size);
                m_total_byte_size += vc.byte_size;
                m_components.push_back(vc);
			}
		}

		void VertexFormat::SetupVertexPointers() const
		{
			GLubyte* ptr = 0;
			for (GLuint i = 0; i < static_cast<GLuint>(m_components.size()); i++)
			{
				const VertexComponent& c = m_components[i];

				glEnableVertexAttribArray(i);
				if (c.integer) {
					glVertexAttribIPointer(i, c.size, c.type, static_cast<GLsizei>(m_total_byte_size), ptr + c.offset);
				} else {
					glVertexAttribPointer(i, c.size, c.type, c.normalized, static_cast<GLsizei>(m_total_byte_size), ptr + c.offset);
				}
			}
		}
	}
}
