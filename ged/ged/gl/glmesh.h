#pragma once

#include "gl.h"
#include "glvertex.h"
#include "glindex.h"
#include "glbuffer.h"
#include "../graphics/mesh.h"
#include "../graphics/vertex.h"
#include "../graphics/index.h"

namespace ged
{
	namespace gl
	{
        class Mesh : public ged::Mesh
		{
		public:
			Mesh(const ged::VertexFormatDescriptor& vfd,
                 const ged::IndexFormatDescriptor& ifd,
                 const void* vertex_data = nullptr,
                 size_t v_data_byte_size = 0,
                 const void* index_data = nullptr,
                 size_t i_data_count = 0,
                 const Group* group_data = nullptr,
                 size_t group_count = 0,
                 const AABB* aabb_data = nullptr,
                 size_t aabb_count = 0);

	        virtual void SetVertexData(const void* data, size_t byte_size) override;
			virtual void* MapVertexData() override;
	        virtual void UnmapVertexData() override;

	        virtual void SetIndexData(const void* data, size_t byte_size) override;
	        virtual void* MapIndexData() override;
	        virtual void UnmapIndexData() override;

	        
	        const VertexFormat&        GetVertexFormat() const;
	        const VertexArrayObject&   GetVertexArrayObject() const;
	        const VertexBuffer&        GetVertexBuffer() const;

            const IndexFormat&         GetIndexFormat() const;
	        const IndexBuffer&         GetIndexBuffer() const;
            GLuint                     GetIndexCount() const;

		private:
			const VertexFormat m_vertex_format;
            const IndexFormat m_index_format;

			VertexArrayObject m_vertex_array_object;
			VertexBuffer m_vertex_buffer;
			IndexBuffer m_index_buffer;
			GLuint m_index_count;
		};

        inline GLuint Mesh::GetIndexCount() const
        {
            return m_index_count;
        }

        inline const VertexFormat& Mesh::GetVertexFormat() const
        {
            return m_vertex_format;
        }

        inline const IndexFormat& Mesh::GetIndexFormat() const
        {
            return m_index_format;
        }

        inline const VertexArrayObject& Mesh::GetVertexArrayObject() const
        {
            return m_vertex_array_object;
        }

        inline const VertexBuffer& Mesh::GetVertexBuffer() const
        {
            return m_vertex_buffer;
        }

        inline const IndexBuffer& Mesh::GetIndexBuffer() const
        {
            return m_index_buffer;
        }
	}
}