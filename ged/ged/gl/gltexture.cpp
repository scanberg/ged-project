#include "gltexture.h"

/*
GED_FORMAT_R8,
GED_FORMAT_RG8,
GED_FORMAT_RGB8,
GED_FORMAT_RGBA8,
GED_FORMAT_R16F,
GED_FORMAT_RG16F,
GED_FORMAT_RGBA16F,
GED_FORMAT_R32F,
GED_FORMAT_RG32F,
GED_FORMAT_RGB32F,
GED_FORMAT_RGBA32F,
GED_FORMAT_R8_COMPRESSED,
GED_FORMAT_RG8_COMPRESSED,
GED_FORMAT_RGB8_COMPRESSED,
GED_FORMAT_RGBA8_COMPRESSED,
GED_FORMAT_DEPTH

*/

static const GLenum GLInternalFormatTable[]
{
    GL_R8, GL_RG8, GL_RGB8, GL_RGBA8,
    GL_R16F, GL_RG16F, GL_RGB16F, GL_RGBA16F,
    GL_R32F, GL_RG32F, GL_RGB32F, GL_RGBA32F,
    GL_COMPRESSED_RED, GL_COMPRESSED_RG, GL_COMPRESSED_RGB, GL_COMPRESSED_RGBA,
    GL_DEPTH_COMPONENT
};

static const GLenum GLFormatTable[]
{
    GL_RED, GL_RG, GL_RGB, GL_RGBA,
    GL_RED, GL_RG, GL_RGB, GL_RGBA,
    GL_RED, GL_RG, GL_RGB, GL_RGBA,
    GL_RED, GL_RG, GL_RGB, GL_RGBA,
    GL_RED
};

static const GLenum GLTypeTable[]
{
    GL_UNSIGNED_BYTE, GL_UNSIGNED_BYTE, GL_UNSIGNED_BYTE, GL_UNSIGNED_BYTE,
    GL_HALF_FLOAT, GL_HALF_FLOAT, GL_HALF_FLOAT, GL_HALF_FLOAT,
    GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT,
    GL_UNSIGNED_BYTE, GL_UNSIGNED_BYTE, GL_UNSIGNED_BYTE, GL_UNSIGNED_BYTE,
    GL_FLOAT
};

static_assert(sizeof(GLInternalFormatTable) / sizeof(GLenum) ==
    static_cast<size_t>(ged::TextureFormat::GED_NUM_TEXTURE_FORMATS),
    "GL Texture Format table size mismatch");

static_assert(sizeof(GLFormatTable) / sizeof(GLenum) ==
    static_cast<size_t>(ged::TextureFormat::GED_NUM_TEXTURE_FORMATS),
    "GL Texture Format table size mismatch");

static_assert(sizeof(GLTypeTable) / sizeof(GLenum) ==
    static_cast<size_t>(ged::TextureFormat::GED_NUM_TEXTURE_FORMATS),
    "GL Texture Format table size mismatch");

namespace ged
{
    namespace gl
    {
        Texture::Texture(const TextureDescriptor& tex_desc, GLenum target) :
            ged::Texture(tex_desc),
            m_target(target)
        {
            glGenTextures(1, &m_id);
        }

        Texture::~Texture()
        {
            glDeleteTextures(1, &m_id);
        }

        void Texture::BindToSlot(u32 slot) const
        {
            assert(slot < 16);
            glActiveTexture(GL_TEXTURE0 + slot);
            glBindTexture(m_target, m_id);
        }

        Texture1D::Texture1D(const TextureDescriptor& tex_desc, const void* data) :
            Texture(tex_desc, GL_TEXTURE_1D)
        {
            GLenum internal_format = GLInternalFormatTable[static_cast<i32>(GetFormat())];
            glBindTexture(GL_TEXTURE_1D, GetID());
            glTexStorage1D(GL_TEXTURE_1D, GetMipLevels(), internal_format, GetWidth());
            glBindTexture(GL_TEXTURE_1D, 0);

            if (data)
                SetData(data, 0);
        }

        void Texture1D::SetData(const void* data, int level)
        {
            GLenum format = GLFormatTable[static_cast<i32>(GetFormat())];
            GLenum type = GLTypeTable[static_cast<i32>(GetFormat())];
            glBindTexture(GL_TEXTURE_1D, GetID());
            glTexSubImage1D(GL_TEXTURE_1D, level, 0, GetWidth(), format, type, data);
            glBindTexture(GL_TEXTURE_1D, 0);
        }

        void Texture1D::GetData(void*, int)
        {
            // TODO implement
            assert(false);
        }

        Texture2D::Texture2D(const TextureDescriptor& tex_desc, const void* data) :
            Texture(tex_desc, GL_TEXTURE_2D)
        {
            GLenum internal_format = GLInternalFormatTable[static_cast<i32>(GetFormat())];
            glBindTexture(GL_TEXTURE_2D, GetID());
            glTexStorage2D(GL_TEXTURE_2D, GetMipLevels(), internal_format, GetWidth(), GetHeight());
           
            if (GetMipLevels() > 1) {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            }
            else {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            }

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
            glBindTexture(GL_TEXTURE_2D, 0);

            if (data)
                SetData(data, 0);
        }

        void Texture2D::SetData(const void* data, int level)
        {
            GLenum format = GLFormatTable[static_cast<i32>(GetFormat())];
            GLenum type = GLTypeTable[static_cast<i32>(GetFormat())];
            glBindTexture(GL_TEXTURE_2D, GetID());
            glTexSubImage2D(GL_TEXTURE_2D, level, 0, 0, GetWidth(), GetHeight(), format, type, data);
            if (GetMipLevels() > 1)
                glGenerateMipmap(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        void Texture2D::GetData(void*, int)
        {
            assert(false);
        }

        Texture3D::Texture3D(const TextureDescriptor& tex_desc, const void* data) :
            Texture(tex_desc, GL_TEXTURE_3D)
        {
            GLenum internal_format = GLInternalFormatTable[static_cast<i32>(GetFormat())];
            glTexStorage3D(GL_TEXTURE_3D, GetMipLevels(), internal_format, GetWidth(), GetHeight(), GetDepth());

            if (data)
                SetData(data, 0);
        }

        void Texture3D::SetData(const void* data, int level)
        {
            GLenum format = GLFormatTable[static_cast<i32>(GetFormat())];
            GLenum type = GLTypeTable[static_cast<i32>(GetFormat())];
            glBindTexture(GL_TEXTURE_3D, GetID());
            glTexSubImage3D(GL_TEXTURE_3D, level, 0, 0, 0, GetWidth(), GetHeight(), GetDepth(), format, type, data);
        }

        void Texture3D::GetData(void*, int)
        {
            assert(false);
        }
    }
}
