#pragma once
#include <string>

namespace ged
{
    std::string GetPath(const std::string& str);
    std::string GetFile(const std::string& str);
    std::string GetFileExtension(const std::string& str);

    bool FileExists(const std::string& path_to_file);
}
