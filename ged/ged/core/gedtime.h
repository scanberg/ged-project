#pragma once
#include <chrono>
#include "common.h"

namespace ged
{
    using TimeStamp = std::chrono::high_resolution_clock::time_point;

    inline TimeStamp GetTimeStamp()
    {
        return std::chrono::high_resolution_clock::now();
    }
    
    f32 GetDeltaTimeF32(TimeStamp t0, TimeStamp t1);
    f64 GetDeltaTimeF64(TimeStamp t0, TimeStamp t1);

    u64 GetDeltaTimeNanoSeconds(TimeStamp t0, TimeStamp t1);

    f64 GetTime();
    u32 GetTimeSeconds();
    u32 GetTimeMilliSeconds();
    u64 GetTimeNanoSeconds();

    f32 GetFrameTime();
    void UpdateFrameTime();
}