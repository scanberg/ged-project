#include "input.h"
#include "../debug/profiling.h"

namespace ged
{
    InputManager* InputManager::s_instance = nullptr;

    const StringID PROFILE_INPUT_STATE_UPDATE = SID("PROFILE STATE UPDATE");
    const StringID PROFILE_INPUT_CREATE_KEY_EVENTS = SID("PROFILE CREATE KEY EVENTS");
    const StringID PROFILE_INPUT_CREATE_MOUSE_EVENTS = SID("PROFILE CREATE MOUSE EVENTS");

    InputManager::InputManager() :
        m_updater(nullptr)
    {
        s_instance = this;
    }

    InputManager::~InputManager()
    {
        s_instance = nullptr;
    }

    void InputManager::Init(const InputStateUpdater& updater)
    {
        m_updater = &updater;

        RegisterProcessName(PROFILE_INPUT_STATE_UPDATE, "INPUT STATE UPDATE");
        RegisterProcessName(PROFILE_INPUT_CREATE_KEY_EVENTS, "INPUT CREATE KEYBOARD EVENTS");
        RegisterProcessName(PROFILE_INPUT_CREATE_MOUSE_EVENTS, "INPUT CREATE MOUSE EVENTS");
    }

    void InputManager::Shutdown()
    {

    }

    void InputManager::UpdateStatesAndCreateEvents(EventQueue* queue)
    {

        KeyboardState prev_keyboard = m_state.keyboard;
        MouseState prev_mouse = m_state.mouse;
        vec2 prev_mouse_pos = m_state.mouse_pos;
        m_state.mouse_scroll = vec2(0.f);

        m_updater->UpdateInputState(&m_state);

        KeyboardState keyboard_change = m_state.keyboard ^ prev_keyboard;
        MouseState mouse_state_change = m_state.mouse ^ prev_mouse;

        if (keyboard_change.any() || m_state.keyboard.any())
        {
            for (i32 i = 0; i < NUM_KEYCODES; i++)
            {
                KeyCode code = static_cast<KeyCode>(i);

                if (keyboard_change[i]) {
                    auto event = std::make_shared<KeyboardEvent>(KeyboardEvent(code));
                    if (m_state.keyboard[i] == true)
                        event->action = ACTION_HIT;
                    else
                        event->action = ACTION_RELEASE;
                    queue->QueueEventMultiTarget(event, GameObject::GetConstManager());
                    //printf("keyboard event \n");
                }
                else if (m_state.keyboard[i]) {
                    auto event = std::make_shared<KeyboardEvent>(KeyboardEvent(code, ACTION_HOLD));
                    queue->QueueEventMultiTarget(event, GameObject::GetConstManager());
                    //printf("keyboard event \n");
                }
            }
        }

        if (mouse_state_change.any())
        {
            for (i32 i = 0; i < NUM_MOUSE_BUTTONS; i++)
            {
                MouseButton button = static_cast<MouseButton>(i);

                if (mouse_state_change[i]) {
                    auto event = std::make_shared<MouseButtonEvent>(MouseButtonEvent(m_state, button));
                    queue->QueueEventMultiTarget(event, GameObject::GetConstManager());
                }
            }
        }

        if (m_state.mouse_pos != prev_mouse_pos) {
            auto event = std::make_shared<MouseMoveEvent>(MouseMoveEvent(m_state, prev_mouse_pos));
            queue->QueueEventMultiTarget(event, GameObject::GetConstManager());
        }

        if (m_state.mouse_scroll != vec2(0.f)) {
            auto event = std::make_shared<MouseScrollEvent>(MouseScrollEvent(m_state));
            queue->QueueEventMultiTarget(event, GameObject::GetConstManager());
        }
    }

    bool InputManager::KeyDown(KeyCode code)
    {
        assert(code > -1 && code < NUM_KEYCODES);
        return m_state.keyboard[code];
    }

    bool InputManager::MouseButtonDown(MouseButton btn)
    {
        assert(btn > -1 && btn < NUM_MOUSE_BUTTONS);
        return m_state.mouse[btn];
    }

    vec2 InputManager::MouseScrollDelta()
    {
        return m_state.mouse_scroll;
    }

    vec2 InputManager::MousePos()
    {
        return m_state.mouse_pos;
    }

    const GameControllerState& InputManager::GetController(u32)
    {
        // Not implemented!
        assert(false);
        return m_state.controllers[0];
    }
}