#pragma once

#include <type_traits>

/**
 *  @brief A type that depends on an arbitrary boolean predicate; simple wrapper of std::enable_if.
 *  Does not compile if the first parameter equals false.
 */
template<bool B, typename T>
using predicate_type = typename std::enable_if<B,T>::type;

/**
 *  @brief A type that depends on an inheritance relation. Basically an enable_if type with
 *  std::is_base_of as the boolean predicate. Decays to the type T if Derived inherits Base.
 */
template<typename Base, typename Derived, typename T>
using dependant_type = predicate_type<
    std::is_base_of<Base,Derived>::value,
    T
>;