#pragma once

#include "common.h"

namespace ged
{
    u32 CRC32(const char* buff, size_t len);
}