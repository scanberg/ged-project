//
//  gedmath.h
//  ged-framework
//
//  Created by Robin Skånberg on 2015-08-22.
//  Copyright (c) 2015 Visual Communications Group Ulm. All rights reserved.
//

#pragma once
#define GLM_SWIZZLE

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/random.hpp>

namespace ged
{
    using glm::vec2;
    using glm::vec3;
    using glm::vec4;

    using glm::ivec2;
    using glm::ivec3;
    using glm::ivec4;

    using glm::uvec2;
    using glm::uvec3;
    using glm::uvec4;

    using glm::quat;

    using glm::mat2;
    using glm::mat3;
    using glm::mat4;

    using glm::abs;
    using glm::dot;
    using glm::cross;
    using glm::clamp;
    using glm::normalize;
    using glm::length;
    using glm::min;
    using glm::max;
    using glm::floor;
    using glm::round;
    using glm::ceil;
    using glm::fract;
    using glm::mix;
    using glm::log;
    using glm::log2;
    using glm::lerp;
    using glm::slerp;
    
    inline float rnd() { return glm::linearRand(0.f, 1.f); }

    const float PI = glm::pi<float>();
    const float TWO_PI = 2.f * PI;
    const float PI_HALF = 0.5f * PI;
    const float PI_THIRD = PI / 3.f;
    const float ONE_OVER_PI = 1.f / PI;
    const float ONE_OVER_TWO_PI = 1.f / TWO_PI;

    const float SQRT_TWO = sqrtf(2.f);
    const float ONE_OVER_SQRT_TWO = 1.f / SQRT_TWO;

}