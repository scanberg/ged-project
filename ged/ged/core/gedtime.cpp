#include "gedtime.h"

namespace ged
{
    static TimeStamp t0 = GetTimeStamp();
    static f32 dt = 0.f;

    f64 GetTime()
    {
        TimeStamp t = GetTimeStamp();
        return static_cast<f64>(std::chrono::duration_cast<std::chrono::nanoseconds>(t - t0).count()) * 0.000000001;
    }

    f32 GetFrameTime()
    {
        return dt;
    }

    u32 GetTimeSeconds()
    {
        TimeStamp t = GetTimeStamp();
        return static_cast<u32>(std::chrono::duration_cast<std::chrono::seconds>(t - t0).count());
    }

    u32 GetTimeMilliSeconds()
    {
        TimeStamp t = GetTimeStamp();
        return static_cast<u32>(std::chrono::duration_cast<std::chrono::milliseconds>(t - t0).count());
    }

    u64 GetTimeNanoSeconds()
    {
        TimeStamp t = GetTimeStamp();
        return static_cast<u64>(std::chrono::duration_cast<std::chrono::nanoseconds>(t - t0).count());
    }

    u64 GetDeltaTimeNanoSeconds(TimeStamp t0, TimeStamp t1)
    {
        return static_cast<u64>(std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count());
    }

    void UpdateFrameTime()
    {
        static f64 old_t = GetTime();
        f64 t = GetTime();
        dt = static_cast<float>((t - old_t));
        old_t = t;
    }
}