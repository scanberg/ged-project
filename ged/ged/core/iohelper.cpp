#include "iohelper.h"

#include <fstream>

namespace ged
{
    std::string GetPath(const std::string& str)
    {
        size_t found = str.find_last_of("/\\");
        return str.substr(0, found + 1);
    }

    std::string GetFile(const std::string& str)
    {
        size_t found = str.find_last_of("/\\");
        return str.substr(found + 1);
    }

    std::string GetFileExtension(const std::string& str)
    {
        size_t found = str.find_last_of(".");
        return str.substr(found + 1);
    }

    bool FileExist(const std::string& path_to_file)
    {
        std::ifstream file(path_to_file, std::ifstream::in);

        if (file.is_open()) {
            file.close();
            return true;
        }

        file.close();
        return false;
    }
}