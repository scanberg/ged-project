#pragma once

namespace ged
{
	class ResourceBase
	{
	public:
        virtual ~ResourceBase() = 0;
    private:
	};
}
