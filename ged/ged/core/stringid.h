#pragma once

#include <cstring>
#include <string>
#include "common.h"
#include "crc32.h"

namespace ged
{
    using StringID = u32;

    inline u32 SID(const char* str)
    {
        return CRC32(str, static_cast<u32>(strlen(str)));
    }

    inline u32 SID(const std::string& str)
    {
        return CRC32(str.c_str(), static_cast<u32>(str.length()));
    }
}