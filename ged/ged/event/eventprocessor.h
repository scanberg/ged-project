#pragma once

#include <memory>
#include "event.h"
#include "../gameobject/gameobject.h"
#include "../core/stringid.h"

/**
* Event Processor
* For processing of game logic
*
* Spock Approves
*
*
*            :                                 :
*           :                                   :
*           :  RRVIttIti+==iiii++iii++=;:,       :
*           : IBMMMMWWWWMMMMMBXXVVYYIi=;:,        :
*           : tBBMMMWWWMMMMMMBXXXVYIti;;;:,,      :
*           t YXIXBMMWMMBMBBRXVIi+==;::;::::       ,
*          ;t IVYt+=+iIIVMBYi=:,,,=i+=;:::::,      ;;
*          YX=YVIt+=,,:=VWBt;::::=,,:::;;;:;:     ;;;
*          VMiXRttItIVRBBWRi:.tXXVVYItiIi==;:   ;;;;
*          =XIBWMMMBBBMRMBXi;,tXXRRXXXVYYt+;;: ;;;;;
*           =iBWWMMBBMBBWBY;;;,YXRRRRXXVIi;;;:;,;;;=
*            iXMMMMMWWBMWMY+;=+IXRRXXVYIi;:;;:,,;;=
*            iBRBBMMMMYYXV+:,:;+XRXXVIt+;;:;++::;;;
*            =MRRRBMMBBYtt;::::;+VXVIi=;;;:;=+;;;;=
*             XBRBBBBBMMBRRVItttYYYYt=;;;;;;==:;=
*              VRRRRRBRRRRXRVYYIttiti=::;:::=;=
*               YRRRRXXVIIYIiitt+++ii=:;:::;==
*               +XRRXIIIIYVVI;i+=;=tt=;::::;:;
*                tRRXXVYti++==;;;=iYt;:::::,;;
*                 IXRRXVVVVYYItiitIIi=:::;,::;
*                  tVXRRRBBRXVYYYIti;::::,::::
*                   YVYVYYYYYItti+=:,,,,,:::::;
*                   YRVI+==;;;;;:,,,,,,,:::::::
*/

namespace ged
{
    class EventQueue;
    
    class EventProcessor
    {
    public:
        EventProcessor(const std::initializer_list<StringID>& accepted_events) :
            m_accepted_events(accepted_events)
        {}

        virtual ~EventProcessor() {}

        virtual void Process(const std::shared_ptr<Event>& event_ptr, GameObject target, EventQueue* queue) = 0;
        bool AcceptsEvent(const std::shared_ptr<Event>& event);
    private:
        const std::vector<StringID> m_accepted_events;
    };

    inline bool EventProcessor::AcceptsEvent(const std::shared_ptr<Event>& event)
    {
        for (StringID id : m_accepted_events)
        {
            if (id == event->id)
                return true;
        }
        return false;
    }
}