#pragma once

#include <deque>
#include <memory>
#include "event.h"
#include "eventcomponent.h"
#include "../gameobject/gameobject.h"

namespace ged
{
    class EventQueue
    {
    public:
        EventQueue();

        size_t size() const;

        void QueueEventSingleTarget(std::shared_ptr<Event> event_ptr, GameObject target, u32 delay_in_millisec = 0);
        //void QueueEventMultiTarget(const std::shared_ptr<Event>& event_ptr, const std::vector<GameObject>& targets, u32 delay_in_millisec = 0);

        template<typename GAMEOBJECT_CONTAINER>
        void QueueEventMultiTarget(const std::shared_ptr<Event>& event_ptr, const GAMEOBJECT_CONTAINER& targets, u32 delay_in_millisec = 0)
        {
            for (const GameObject& target : targets)
                QueueEventSingleTarget(event_ptr, target, delay_in_millisec);
        }

        void ProcessEvents();

    private:
        struct QueuedEvent
        {
            EventProcessor* processor;
            std::shared_ptr<Event> event_ptr;
            GameObject target;
            u32 target_time;
        };

        std::deque<QueuedEvent> m_event_queue;
    };

    inline size_t EventQueue::size() const
    {
        return m_event_queue.size();
    }
}