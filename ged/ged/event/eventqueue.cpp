#include <algorithm>
#include "eventqueue.h"
#include "../core/gedtime.h"

namespace ged
{
    EventQueue::EventQueue()
    {
    }

    void EventQueue::QueueEventSingleTarget(std::shared_ptr<Event> event_ptr, GameObject target, u32 delay_in_millisec)
    {
        auto ec = target.GetComponent<EventComponent>();

        // Check that the target can receive events
        if (ec == nullptr || !ec->HasProcessorBound() || !ec->GetProcessor()->AcceptsEvent(event_ptr))
            return;

        QueuedEvent qe{ ec->GetProcessor(), event_ptr, target, GetTimeMilliSeconds() + delay_in_millisec };
        m_event_queue.push_back(qe);
    }
/*
    void EventQueue::QueueEventMultiTarget(const std::shared_ptr<Event>& event_ptr, const std::vector<GameObject>& targets, u32 delay_in_millisec)
    {
        for (auto target : targets)
            QueueEventSingleTarget(event_ptr, target, delay_in_millisec);
    }

    void EventQueue::QueueEventMultiTarget(const std::shared_ptr<Event>& event_ptr, const GameObjectManager& targets, u32 delay_in_millisec)
    {
        for (const auto& target : targets)
            QueueEventSingleTarget(event_ptr, target, delay_in_millisec);
    }
    */
    void EventQueue::ProcessEvents()
    {
        if (m_event_queue.size() > 0)
        {
            // Sort on time
            std::sort(m_event_queue.begin(), m_event_queue.end(),
                [](const QueuedEvent& a, const QueuedEvent& b)
            {
                // Compare target time for processing
                return a.target_time < b.target_time;
            });

            // Get a current time stamp
            u32 curr_time = GetTimeMilliSeconds();

            // Store the size before we kick off any events
            // (To make sure we don't process any events that
            // are created by the processed events)
            size_t queue_size = m_event_queue.size();

            // process the queue and dispatch events to until we find a
            // timestamp that is not yet ready for processing.
            for (size_t count = 0; count < queue_size; count++)
            {
                // Get the front of the queue
                QueuedEvent& qe = m_event_queue.front();

                // Is it time to process the next queued event?
                if (qe.target_time <= curr_time)
                {
                    // Process event
                    qe.processor->Process(qe.event_ptr, qe.target, this);
                }
                else
                    break;

                // Remove the processed element from the queue
                m_event_queue.pop_front();
            }
        }
    }
}