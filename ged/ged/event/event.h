#pragma once
#include <typeindex>
#include "../core/common.h"

namespace ged
{
    const u32 UNDEFINED_EVENT = static_cast<u32>(-1);

    struct Event
    {
        Event(u32 id = UNDEFINED_EVENT) :
            id(id)
        {}

        const u32 id;
    };
}