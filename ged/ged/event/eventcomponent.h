#pragma once

#include <memory>
#include "event.h"
#include "eventprocessor.h"
#include "../gameobject/gameobject.h"

namespace ged
{
    /**
     * EventComponent
     * In charge of processing events being passed to the game object.
     * The function pointer attached to the component determines how
     * the event is processed.
     */
    class EventComponent : public Component<EventComponent>
    {
    public:
        EventComponent(std::shared_ptr<EventProcessor> processor) :
            m_processor(processor)
        {}

        ~EventComponent()
        {}

        void SetProcessor(std::shared_ptr<EventProcessor> processor)
        {
            m_processor = processor;
        }

        EventProcessor* GetProcessor() const
        {
            return m_processor.get();
        }

        bool HasProcessorBound() const
        {
            return m_processor != nullptr;
        }
    private:
        std::shared_ptr<EventProcessor> m_processor;
    };
}