#include <fstream>
#include <sstream>
#include "core/iohelper.h"
#include "graphics/graphicsdevice.h"
#include "material.h"

/*
GED_BASE_COLOR,
GED_NORMAL,
GED_ALPHA,
GED_SMOOTHNESS,
GED_REFLECTANCE,
GED_METAL_MASK,
GED_DETAIL,
GED_EMISSION
*/

bool IsMaterialComponentType(const std::string& str)
{
    return (
        str == "BASE_COLOR" ||
        str == "NORMAL" ||
        str == "ALPHA" ||
        str == "SMOOTHNESS" ||
        str == "REFLECTANCE" ||
        str == "METAL_MASK" ||
        str == "DETAIL" ||
        str == "EMISSION"
        );
}

ged::PBRMaterialComponent GetComponent(const std::string& str)
{
    if (str == "BASE_COLOR") return ged::PBRMaterialComponent::GED_BASE_COLOR;
    if (str == "NORMAL") return ged::PBRMaterialComponent::GED_NORMAL;
    if (str == "ALPHA") return ged::PBRMaterialComponent::GED_ALPHA;
    if (str == "SMOOTHNESS") return ged::PBRMaterialComponent::GED_SMOOTHNESS;
    if (str == "REFLECTANCE") return ged::PBRMaterialComponent::GED_REFLECTANCE;
    if (str == "METAL_MASK") return ged::PBRMaterialComponent::GED_METAL_MASK;
    if (str == "EMISSION") return ged::PBRMaterialComponent::GED_EMISSION;

    return ged::PBRMaterialComponent::GED_UNDEFINED;
}

namespace ged
{
    std::vector<Material*> LoadMaterials(const GraphicsDevice& gfx_dev,
                                         const char* filename,
                                         std::map<std::string, size_t>* mat_idx_map)
    {
        std::vector<Material*> materials;

        std::ifstream file(filename, std::ifstream::in);
        if (!file.is_open())
        {
            printf("There was a problem opening the material file: '%s' \n", filename);
            return materials;
        }

        std::string path = GetPath(filename);

        std::string line;
        Material* current_material = nullptr;

        while (file.good() && !file.eof()) {
            std::getline(file, line);
            if (line[0] == '#' || line[0] == '\n')
                continue;

            std::stringstream ss(line);
            std::string word;
            ss >> word;

            if (word == "MATERIAL") {
                // fetch word that contains name of material
                ss >> word;

                // if map is passed, add the id of this material
                if (mat_idx_map != nullptr)
                    (*mat_idx_map)[word] = materials.size();

                current_material = new Material();
                materials.push_back(current_material);
                continue;
            }

            if (IsMaterialComponentType(word)) {
                if (current_material == nullptr)
                    continue;

                std::string type, texture;
                vec4 color;

                type = word;
                ss >> texture;
                ss >> color.x; ss >> color.y; ss >> color.z; ss >> color.w;

                PBRMaterialComponent mat_comp_id = GetComponent(type);
                if (mat_comp_id == PBRMaterialComponent::GED_UNDEFINED)
                    continue;

                Texture* tex = LoadTexture(gfx_dev, (path + texture).c_str());

                Material::Component mat_comp(tex, color);
                current_material->SetComponent(mat_comp_id, mat_comp);
            }
        }

        file.close();

        return std::move(materials);
    }
}
