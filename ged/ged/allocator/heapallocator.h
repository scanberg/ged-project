#pragma once
#include "allocator.h"

namespace ged
{
    /**
     * A simple wrapper around the default heap allocator provided by malloc & free
     */
    class HeapAllocator : public Allocator
    {
    public:
        // Alignment disregarded!
        void* Allocate(size_t size, size_t) final
        {
            return malloc(size);
        }
        
        void Deallocate(void* ptr) final
        {
            free(ptr);
        }
    };
};