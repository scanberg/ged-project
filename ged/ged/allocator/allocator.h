#pragma once

#include "../core/common.h"

namespace ged
{
    /**
     *  The interface used for a custom allocator
     */
    class Allocator
    {
    public:
        virtual ~Allocator() {}
        virtual void* Allocate(size_t size, size_t align) = 0;
        virtual void Deallocate(void* ptr) = 0;

        template <class T, typename... Args>
        T* New(Args&&... args)
        {
#ifdef _MSC_VER
            // Special case for visual studio >.<, does not support alignof until VS15
            size_t align = __alignof(T);
#else
            size_t align = alignof(T);
#endif
            return new (Allocate(sizeof(T), align)) T(std::forward<Args>(args)...);
        }

        template <class T>
        void Delete(T* p)
        {
            if (p) {
                p->~T();
                Deallocate(p);
            }
        }
    };
}