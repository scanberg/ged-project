#pragma once

#include <memory>
#include "core/common.h"
#include "core/gedmath.h"
#include "core/stringid.h"
#include "geom/aabb.h"
#include "graphics/graphicsdevice.h"
#include "graphics/renderer.h"
#include "graphics/mesh.h"
#include "material.h"

namespace ged
{
    class Mesh;

    struct GroupMaterialMapping
    {
        GroupMaterialMapping(size_t idx = 0, Material* mat = nullptr) :
            group_idx(idx),
            material(mat)
        {}

        inline bool operator < (const GroupMaterialMapping& other) const
        {
            return group_idx < other.group_idx;
        }

        size_t group_idx;
        Material* material;
    };

	class Model
	{
	public:
		inline void	SetMesh(const Mesh* mesh)
        {
            m_mesh = mesh;
            m_grp_mat_mappings.clear();

            /*
            if (mesh != nullptr)
            {
                m_materials.resize(m_mesh->GetGroupSize(), nullptr);
                m_group_flags.resize(m_mesh->GetGroupSize(), true);
            }
            else
            {
                m_materials.clear();
                m_group_flags.clear();
            }
            */

            UpdateAABB();
        }

		inline void SetMesh(std::shared_ptr<Mesh> const& mesh)
		{
            SetMesh(mesh.get());
		}

		inline const Mesh* GetMesh() const
		{
			return m_mesh;
		}

        inline const AABB& GetAABB() const
        {
            return m_aabb;
        }

        /*
        inline const std::vector<Material*>& GetMaterials() const
        {
            return m_materials;
        }
        */

        inline const std::vector<GroupMaterialMapping>& GetGroupMaterialMappings() const
        {
            return m_grp_mat_mappings;
        }

        inline void ClearGroupMaterialMappings()
        {
            m_grp_mat_mappings.clear();
        }

        void MapMaterialToAllGroups(Material* material);
        void MapMaterialToGroup(Material* material, const StringID& id);
        void MapMaterialToGroupIdx(Material* material, size_t idx);
        /*
        const std::vector<bool>& GetGroupFlags() const
        {
            return m_group_flags;
        }

        bool GetGroupFlag(size_t group_idx)
        {
            if (group_idx >= m_group_flags.size()) {
                // TODO log error
                printf("group enable idx out of bounds \n");
                return false;
            }

            return m_group_flags[group_idx];
        }

        void SetAllGroupFlags(bool flag)
        {
            for (size_t i = 0; i < m_group_flags.size(); i++)
                m_group_flags[i] = flag;
            UpdateAABB();
        }

        void SetGroupFlags(const std::vector<bool>& flags)
        {
            size_t min_size = min(flags.size(), m_group_flags.size());
            for (size_t i = 0; i < min_size; i++)
                m_group_flags[i] = flags[i];
            UpdateAABB();
        }

        void SetGroupFlag(size_t group_idx, bool flag)
        {
            if (group_idx >= m_group_flags.size()) {
                // TODO log error
                printf("group enable idx out of bounds \n");
                return;
            }

            m_group_flags[group_idx] = flag;
            UpdateAABB();
        }
        */
	private:
        friend Model* LoadModel(const GraphicsDevice& gfx_dev, const char* filename);

        void UpdateAABB();

        AABB m_aabb;
		const Mesh* m_mesh;
		//std::vector<Material*> m_materials;
        std::vector<GroupMaterialMapping> m_grp_mat_mappings;

        //std::vector<bool> m_group_flags;
	};

    Model* LoadModel(const GraphicsDevice& gfx_dev, const char* filename);
    inline Model* LoadModel(const GraphicsDevice& gfx_dev, const std::string& filename)
    {
        return LoadModel(gfx_dev, filename.c_str());
    }
}