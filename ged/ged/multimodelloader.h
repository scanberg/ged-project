#pragma once
#include <vector>
#include <string>
#include <ged/gameobject/gameobject.h>
#include <ged/graphics/graphicsdevice.h>

namespace ged
{
    std::vector<Model*> LoadMultiModel(const GraphicsDevice& gfx_dev, const char* filename);

    inline std::vector<Model*> LoadMultiModel(const GraphicsDevice& gfx_dev, const std::string& filename)
    {
        return LoadMultiModel(gfx_dev, filename.c_str());
    }
}