#pragma once
#include <initializer_list>
#include <string>
#include "../core/resource.h"

namespace ged
{
    enum class ShaderStageType
    {
        GED_VERTEX_SHADER,
        GED_FRAGMENT_SHADER,
        GED_GEOMETRY_SHADER,
        GED_COMPUTE_SHADER,
        GED_TESS_CTRL_SHADER,
        GED_TESS_EVAL_SHADER
    };

    struct ShaderStage
    {
        ShaderStage(ShaderStageType type, const char* filename) :
            type(type), filename(filename) {}
        ShaderStage(ShaderStageType type, const std::string& filename) :
            type(type), filename(filename.c_str()) {}

        ShaderStageType type;
        std::string filename;
    };

	class Shader: public ResourceBase
	{
	public:
        Shader(const std::initializer_list<ShaderStage>& list) :
            m_stage_list(list) {}
		virtual void Activate() const = 0;

    protected:
        std::initializer_list<ShaderStage> m_stage_list;
	};

    //Shader* LoadShader(const std::initializer_list<ShaderStage>& list);
}