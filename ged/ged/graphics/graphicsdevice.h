#pragma once

#include <cassert>
#include <string>
#include "mesh.h"
#include "texture.h"
#include "shader.h"

namespace ged
{
    class Mesh;
    class Shader;
    class Texture;
    class RenderTargetBase;

    struct GraphicsConfig
    {
        int resolution_x = 1024;
        int resolution_y = 768;
        int fullscreen = 0;
        int vertical_sync = 1;
        std::string title = "GED Window";
    };

    enum ClearBufferTarget
    {
        GED_COLOR_BUFFER,
        GED_DEPTH_BUFFER,
        GED_STENCIL_BUFFER,
        GED_ALL_BUFFERS
    };

    class GraphicsDevice
    {
    public:
        GraphicsDevice()
        {
            s_current_device = this;
        }

        virtual void Init(const GraphicsConfig& cfg) = 0;
        virtual void Shutdown() {}

        // Default primitives for drawing
        virtual const Mesh* GetQuad() const = 0;
        virtual const Mesh* GetCube() const = 0;
        virtual const Mesh* GetLineCube() const = 0;

        // Default textures
        virtual const Texture* GetCheckerBoardTexture() const = 0;
        virtual const Texture* GetSolidWhiteTexture() const = 0;

        virtual Mesh* CreateMesh(const VertexFormatDescriptor& vfd,
                                     const IndexFormatDescriptor& ifd,
                                     const void* vertex_data = nullptr,
                                     size_t vertex_data_byte_size = 0,
                                     const void* index_data = nullptr,
                                     size_t index_data_byte_size = 0) const = 0;

        virtual Texture* CreateTexture(const TextureDescriptor& tex_desc, const void* data = nullptr) const = 0;
        virtual Shader* CrateShader(const std::initializer_list<ShaderStage>& list) const = 0;

        virtual void ClearBuffers(ClearBufferTarget target = GED_ALL_BUFFERS) const = 0;
        virtual void FlushCommandQueue() const {};
        virtual void SwapBuffers() const = 0;

        static GraphicsDevice* GetInstance();
    protected:
        static GraphicsDevice* s_current_device;
    };

    inline GraphicsDevice* GraphicsDevice::GetInstance()
    {
        return s_current_device;
    }
}