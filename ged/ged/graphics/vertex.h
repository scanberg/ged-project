#pragma once

#include <vector>
#include <initializer_list>
#include <glm/gtc/packing.hpp>
#include "../core/common.h"
#include "../core/gedmath.h"

namespace ged
{
    enum class VertexComponentFormat
    {
        GED_FORMAT_R32G32B32A32_FLOAT,
        GED_FORMAT_R32G32B32A32_INT,
        GED_FORMAT_R32G32B32A32_UINT,

        GED_FORMAT_R32G32B32_FLOAT,
        GED_FORMAT_R32G32B32_INT,
        GED_FORMAT_R32G32B32_UINT,

        GED_FORMAT_R32G32_FLOAT,
        GED_FORMAT_R32G32_INT,
        GED_FORMAT_R32G32_UINT,

        GED_FORMAT_R32_FLOAT,
        GED_FORMAT_R32_INT,
        GED_FORMAT_R32_UINT,

        GED_FORMAT_R16G16B16A16_FLOAT,
        GED_FORMAT_R16G16B16A16_INT,
        GED_FORMAT_R16G16B16A16_UINT,
        GED_FORMAT_R16G16B16A16_INORM,
        GED_FORMAT_R16G16B16A16_UNORM,

        GED_FORMAT_R16G16_FLOAT,
        GED_FORMAT_R16G16_INT,
        GED_FORMAT_R16G16_UINT,
        GED_FORMAT_R16G16_INORM,
        GED_FORMAT_R16G16_UNORM,

        GED_FORMAT_R8G8B8A8_INT,
        GED_FORMAT_R8G8B8A8_UINT,
        GED_FORMAT_R8G8B8A8_INORM,
        GED_FORMAT_R8G8B8A8_UNORM,

        GED_FORMAT_R8G8_INT,
        GED_FORMAT_R8G8_UINT,
        GED_FORMAT_R8G8_INORM,
        GED_FORMAT_R8G8_UNORM,

        GED_FORMAT_R10G10B10A2_INT,
        GED_FORMAT_R10G10B10A2_UINT,
        GED_FORMAT_R10G10B10A2_INORM,
        GED_FORMAT_R10G10B10A2_UNORM,
        GED_FORMAT_R11G11B10_FLOAT,

        GED_NUM_VERTEX_COMPONENT_FORMATS = 35
    };

    struct VertexFormatDescriptor
    {
        VertexFormatDescriptor(const std::initializer_list<VertexComponentFormat>& list) :
            components(list) {}

        bool operator == (const VertexFormatDescriptor& other) const;

        const std::vector<VertexComponentFormat> components;
    };

    struct VertexPositionOnly
    {
        vec3 position;

        static const VertexFormatDescriptor format;
    };

    struct VertexDefault
    {
        vec3 position;
        vec3 normal;
        vec2 texcoord;

        static const VertexFormatDescriptor format;
    };

    static_assert(sizeof(VertexDefault) == 32, "VertexDefault size is wrong.");

    struct VertexSkinned
    {
        vec3 position;
        u32 normal;
        vec2 texcoord;
        u8 bone_indices[4];
        u8 bone_weights[4];
        
        void SetNormal(const vec3& v);
        void SetBoneIndices(const ivec4& indices);
        void SetBoneWeights(const vec4& weights);

        vec3 GetNormal() const;
        ivec4 GetBoneIndices() const;
        vec4 GetBoneWeights() const;

        static const VertexFormatDescriptor format;
    };

    static_assert(sizeof(VertexSkinned) == 32, "VertexSkinned size is wrong.");

    inline void VertexSkinned::SetNormal(const vec3& v)
    {
        normal = glm::packSnorm3x10_1x2(vec4(v, 0));
    }

    inline void VertexSkinned::SetBoneIndices(const ivec4& indices)
    {
        bone_indices[0] = static_cast<u8>(indices[0]);
        bone_indices[1] = static_cast<u8>(indices[1]);
        bone_indices[2] = static_cast<u8>(indices[2]);
        bone_indices[3] = static_cast<u8>(indices[3]);
    }

    inline void VertexSkinned::SetBoneWeights(const vec4& weights)
    {
        u32* data = reinterpret_cast<u32*>(bone_weights);
        *data = glm::packUnorm4x8(weights);
    }

    inline vec3 VertexSkinned::GetNormal() const
    {
        return vec3(glm::unpackSnorm3x10_1x2(normal));
    }

    inline ivec4 VertexSkinned::GetBoneIndices() const
    {
        return ivec4(bone_indices[0], bone_indices[1], bone_indices[2], bone_indices[3]);
    }

    inline vec4 VertexSkinned::GetBoneWeights() const
    {
        const u32* data = reinterpret_cast<const u32*>(bone_weights);
        return glm::unpackUnorm4x8(*data);
    }
}
