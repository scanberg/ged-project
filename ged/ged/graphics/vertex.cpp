#include "vertex.h"

namespace ged
{
    
    bool VertexFormatDescriptor::operator == (const VertexFormatDescriptor& other) const
    {
        if (components.size() != other.components.size())
            return false;

        for (size_t i = 0; i < components.size(); i++)
            if (components[i] != other.components[i])
                return false;

        return true;
    }

    const VertexFormatDescriptor VertexPositionOnly::format(
    {
        VertexComponentFormat::GED_FORMAT_R32G32B32_FLOAT
    });

    const VertexFormatDescriptor VertexDefault::format(
    {
        VertexComponentFormat::GED_FORMAT_R32G32B32_FLOAT,
        VertexComponentFormat::GED_FORMAT_R32G32B32_FLOAT,
        VertexComponentFormat::GED_FORMAT_R32G32_FLOAT
    });

    const VertexFormatDescriptor VertexSkinned::format(
    {
        VertexComponentFormat::GED_FORMAT_R32G32B32_FLOAT,
        VertexComponentFormat::GED_FORMAT_R10G10B10A2_INORM,
        VertexComponentFormat::GED_FORMAT_R32G32_FLOAT,
        VertexComponentFormat::GED_FORMAT_R8G8B8A8_UINT,
        VertexComponentFormat::GED_FORMAT_R8G8B8A8_UNORM
    });
}
