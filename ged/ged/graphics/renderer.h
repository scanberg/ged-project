#pragma once

#include <string>
#include <iterator>
//#include <map>
#include "../core/common.h"
#include "../gameobject/gameobject.h"
#include "../camera.h"
#include "graphicsdevice.h"


namespace ged
{
    class Model;

	struct RenderComponent : public Component<RenderComponent>
	{
		RenderComponent(Model* m = nullptr) :
			model(m),
			visible(true) {}
		const Model* model;
		bool visible : 1;
	};

	class Renderer
	{
	public:
        virtual void Init(const GraphicsDevice& gfx_dev) = 0;
        virtual void Shutdown() {};
        
        virtual void Render(const std::vector<GameObject>& objects, const CameraParameters& param) = 0;
	};
};