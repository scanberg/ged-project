#pragma once
#include "../core/common.h"
#include "../core/gedmath.h"
#include "../core/resource.h"

namespace ged
{
    enum class TextureType
    {
        GED_TEXTURE_1D,
        GED_TEXTURE_2D,
        GED_TEXTURE_3D,
        GED_TEXTURE_CUBE_MAP
    };

    enum class TextureFormat
    {
        GED_FORMAT_R8,
        GED_FORMAT_RG8,
        GED_FORMAT_RGB8,
        GED_FORMAT_RGBA8,
        GED_FORMAT_R16F,
        GED_FORMAT_RG16F,
        GED_FORMAT_RGB16F,
        GED_FORMAT_RGBA16F,
        GED_FORMAT_R32F,
        GED_FORMAT_RG32F,
        GED_FORMAT_RGB32F,
        GED_FORMAT_RGBA32F,
        GED_FORMAT_R8_COMPRESSED,
        GED_FORMAT_RG8_COMPRESSED,
        GED_FORMAT_RGB8_COMPRESSED,
        GED_FORMAT_RGBA8_COMPRESSED,
        GED_FORMAT_DEPTH,

        GED_FORMAT_UNDEFINED,
        GED_NUM_TEXTURE_FORMATS = GED_FORMAT_UNDEFINED
    };

    struct TextureDescriptor
    {
        TextureDescriptor(TextureType type = TextureType::GED_TEXTURE_2D,
                          TextureFormat format = TextureFormat::GED_FORMAT_RGBA8,
                          u32 width = 16, u32 height = 16, u32 depth = 1,
                          u32 mip_levels = 1) :
            type(type),
            format(format),
            width(width), height(height), depth(depth),
            mip_levels(mip_levels)
        {}

        TextureType type;
        TextureFormat format;

        u32 width;
        u32 height;
        u32 depth;
        u32 mip_levels;
    };

	class Texture: public ResourceBase
	{
	public:
        Texture(const TextureDescriptor& tex_desc) :
            m_tex_desc(tex_desc)
		{}

		virtual ~Texture() = default;

		virtual void BindToSlot(u32 slot) const = 0;
        virtual void SetData(const void* data, int level = 0) = 0;
        virtual void GetData(void* dst, int level = 0) = 0;

        inline TextureType GetType() const;
        inline TextureFormat GetFormat() const;

        inline u32 GetWidth() const;
        inline u32 GetHeight() const;
        inline u32 GetDepth() const;
        inline u32 GetMipLevels() const;
        inline const TextureDescriptor& GetTextureDescriptor() const;

	private:
        const TextureDescriptor m_tex_desc;
	};

    inline TextureType Texture::GetType() const
    {
        return m_tex_desc.type;
    }

    inline TextureFormat Texture::GetFormat() const
    {
        return m_tex_desc.format;
    }

    inline u32 Texture::GetWidth() const
    {
        return m_tex_desc.width;
    }

    inline u32 Texture::GetHeight() const
    {
        return m_tex_desc.height;
    }

    inline u32 Texture::GetDepth() const
    {
        return m_tex_desc.depth;
    }

    inline u32 Texture::GetMipLevels() const
    {
        return m_tex_desc.mip_levels;
    }

    inline const TextureDescriptor& Texture::GetTextureDescriptor() const
    {
        return m_tex_desc;
    }

    class GraphicsDevice;

    Texture* LoadTexture(const GraphicsDevice& gfx_dev, const char* filename);
    inline Texture* LoadTexture(const GraphicsDevice& gfx_dev, const std::string& filename)
    {
        return LoadTexture(gfx_dev, filename.c_str());
    }
}
