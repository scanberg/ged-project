#include "mesh.h"

#include <fstream>
#include <iostream>
#include "../transform.h"
#include "graphicsdevice.h"

using namespace ged;

struct GroupDataItem
{
    char name[32];
    u32 offset;
    u32 length;
};

struct BoneDataItem
{
    char name[32];
    quat q;
    vec3 t;
    vec3 s;
    u32 id;
    i32 parent;
};

struct VertexDataItem
{
    vec3 position;
    vec3 normal;
    vec2 texcoord;
};

struct VertexBoneDataItem
{
    ivec4 indices;
    vec4 weights;
};

struct MeshHeaderBlock
{
    // HEADER

    u64 identifier;
    u32 version;

    u32 num_vertices;
    u32 num_indices;
    u32 num_groups;
    u32 num_bones;

    u32 vertex_data_offset;
    u32 vertex_bone_data_offset;
    u32 index_data_offset;
    u32 group_data_offset;
    u32 bone_data_offset;

    // Data follows here
};

void ComputeGroupAABBs(AABB* aabbs,
                       const GroupDataItem* groups,
                       const u32* indices,
                       const VertexDataItem* vertices,
                       size_t num_groups)
{
    for (size_t g_idx = 0; g_idx < num_groups; g_idx++)
    {
        AABB box(vec3(FLT_MAX), vec3(-FLT_MAX));
        u32 begin = groups[g_idx].offset;
        u32 end = groups[g_idx].offset + groups[g_idx].length;

        for (u32 i_idx = begin; i_idx < end; i_idx++)
        {
            u32 i = indices[i_idx];
            const vec3& v_pos = vertices[i].position;

            box.min_box = min(box.min_box, v_pos);
            box.max_box = max(box.max_box, v_pos);
        }

        aabbs[g_idx] = box;
    }
}

namespace ged
{
    

    Mesh* LoadMesh(const GraphicsDevice& gfx_dev, const char* filename)
    {
        // TODO Check if filename is already registered in resource mgr

        // Open file stream
        std::ifstream file(filename, std::ifstream::binary);
        if (!file.is_open())
        {
            printf("There was a problem opening the mesh file: '%s' \n", filename);
            return nullptr;
        }

        // Read size of file
        file.seekg (0, file.end);
        size_t data_size = file.tellg();
        file.seekg (0, file.beg);

        // Make sure that the file at least contains a header block in size
        assert(data_size >= sizeof(MeshHeaderBlock));

        // Allocate memory and read entire file
        char* data_block = new char[data_size];
        file.read(data_block, data_size);

        // Close file stream
        file.close();

        // Get header from data
        MeshHeaderBlock* header = reinterpret_cast<MeshHeaderBlock*>(data_block);

        // Assert identifier and version of file
        assert(header->identifier == *reinterpret_cast<uint64_t*>(const_cast<char*>("GED_MESH")));
        assert(header->version >= 1000);

        // Compute how many bone vertices there are
        u32 num_bone_vertices = (header->index_data_offset - header->vertex_bone_data_offset) / sizeof(VertexBoneDataItem);

#define GetPointer(type, offset) reinterpret_cast<type>(data_block + offset)

        // Get Pointers to the underlying data
        VertexDataItem*     vertex_data = GetPointer(VertexDataItem*, header->vertex_data_offset);
        VertexBoneDataItem* vertex_bone_data = GetPointer(VertexBoneDataItem*, header->vertex_bone_data_offset);
        u32*                index_data = GetPointer(u32*, header->index_data_offset);
        GroupDataItem*      group_data = GetPointer(GroupDataItem*, header->group_data_offset);
        BoneDataItem*       bone_data = GetPointer(BoneDataItem*, header->bone_data_offset);

#undef GetPointer
        
        // Setup index format descriptor for Triangles of type u32
        IndexFormatDescriptor index_format(IndexType::GED_TYPE_U32, IndexPrimitive::GED_TRIANGLES);

        // The pointer to the mesh about to be created
        Mesh* mesh = nullptr;

        // If we have no additional bone vertices, just read data
        if (num_bone_vertices == 0) {
            // Create mesh using the GraphicsDevice
            mesh = gfx_dev.CreateMesh(
                VertexDefault::format,
                index_format,
                vertex_data,
                header->num_vertices * sizeof(VertexDefault),
                index_data,
                header->num_indices * sizeof(u32));
        }
        else
        {
            // Read additional bone information and concatenate into one Vertex Skinned Structure

            // Assert that the number of bone vertices matches the read vertices
            assert(num_bone_vertices == header->num_vertices);
            std::vector<VertexSkinned> vertex_skinned_data(num_bone_vertices);

            // Setup all bone vertices
            for (u32 i = 0; i < num_bone_vertices; i++)
            {
                vertex_skinned_data[i].position = vertex_data[i].position;
                vertex_skinned_data[i].SetNormal(vertex_data[i].normal);
                vertex_skinned_data[i].texcoord = vertex_data[i].texcoord;
                vertex_skinned_data[i].SetBoneIndices(vertex_bone_data[i].indices);
                vertex_skinned_data[i].SetBoneWeights(vertex_bone_data[i].weights);
                
                ivec4 idx = vertex_skinned_data[i].GetBoneIndices();
                vec4 w = vertex_skinned_data[i].GetBoneWeights();
                
                // Assert that if the index is unused, the weight must be 0
                for (int k = 0; k < 4; k++)
                    if (idx[k] == 65535)
                        assert(w[k] == 0.f);
            }

            // Create the mesh using the graphicsdevice
            mesh = gfx_dev.CreateMesh(
                VertexSkinned::format,
                index_format,
                vertex_skinned_data.data(),
                num_bone_vertices * sizeof(VertexSkinned),
                index_data,
                header->num_indices * sizeof(u32));
        }

        // If we have any optional bones, add them to the mesh
        if (header->num_bones > 0)
        {
            auto& bone_parents = mesh->GetBoneParents();
            auto& bone_offsets = mesh->GetBoneOffsets();
            auto& bone_map = mesh->GetBoneMap();

            bone_parents.reserve(header->num_bones);
            bone_offsets.reserve(header->num_bones);

            for (u32 i = 0; i < header->num_bones; i++)
            {
                const BoneDataItem& bd = bone_data[i];
                StringID sid = SID(bd.name);
                assert(sid == bd.id);

                bone_parents.push_back(bd.parent);
                bone_offsets.push_back(Transform(bd.t, bd.q, bd.s).Matrix());
                bone_map.insert(std::pair<StringID, i32>(sid, static_cast<i32>(i)));

                mesh->m_total_bone_hash += bd.id;
            }
        }

        // Read groups
        auto& groups = mesh->GetGroups();
        auto& aabbs = mesh->GetGroupAABBs();
        auto& group_map = mesh->GetGroupMap();

        groups.resize(header->num_groups);
        aabbs.resize(header->num_groups);

        for (u32 i = 0; i < header->num_groups; i++) {
            groups[i] = Mesh::Group(group_data[i].offset, group_data[i].length);
            group_map.insert(std::pair<StringID, i32>(SID(group_data[i].name), i));
        }

        // Compute AABBs of the groups
        ComputeGroupAABBs(aabbs.data(), group_data, index_data, vertex_data, header->num_groups);

        delete[] data_block;

        return mesh;
    }
}