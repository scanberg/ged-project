#pragma once

namespace ged
{
    enum class IndexType
    {
        GED_TYPE_U8,
        GED_TYPE_U16,
        GED_TYPE_U32
    };

    enum class IndexPrimitive
    {
        GED_POINTS,
        GED_LINES,
        GED_LINES_ADJACENCY,
        GED_LINE_STRIP,
        GED_LINE_STRIP_ADJACENCY,
        GED_LINE_LOOP,
        GED_TRIANGLES,
        GED_TRIANGLES_ADJACENCY,
        GED_TRIANGLE_STRIP,
        GED_TRIANGLE_STRIP_ADJACENCY,
        GED_TRIANGLE_FAN
    };

    struct IndexFormatDescriptor
    {
        IndexFormatDescriptor(IndexType type, IndexPrimitive primitive) :
            type(type), primitive(primitive)
        {}
        
        const IndexType type;
        const IndexPrimitive primitive;
    };
}