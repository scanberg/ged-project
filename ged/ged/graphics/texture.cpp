#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include "../core/gedmath.h"
#include "graphicsdevice.h"
#include "texture.h"

static const ged::TextureFormat TexFormatTable[]
{
    ged::TextureFormat::GED_FORMAT_UNDEFINED,
    ged::TextureFormat::GED_FORMAT_R8,
    ged::TextureFormat::GED_FORMAT_RG8,
    ged::TextureFormat::GED_FORMAT_RGB8,
    ged::TextureFormat::GED_FORMAT_RGBA8
};

/*
static const ged::TextureFormat HDRTexFormatTable[]
{
    ged::GED_FORMAT_UNDEFINED,
        ged::GED_FORMAT_R16F,
        ged::GED_FORMAT_RG16F,
        ged::GED_FORMAT_RGB16F,
        ged::GED_FORMAT_RGBA16F
};
*/

namespace ged
{
    /* 
        TODO: Add support for .HDR files and floating point precision textures
    */
    Texture* LoadTexture(const GraphicsDevice& gfx_dev, const char* filename)
    {
        assert(filename);

        int x, y, n;
        stbi_set_flip_vertically_on_load(1);
        unsigned char *data = stbi_load(filename, &x, &y, &n, 0);

        if (data == nullptr) {
            printf("Could not load texture '%s' \n", filename);
            return nullptr;
        }

        TextureDescriptor desc;
        desc.type = TextureType::GED_TEXTURE_2D;
        desc.format = TexFormatTable[n];
        desc.width = x;
        desc.height = y;
        desc.depth = 1;
        desc.mip_levels = max(1U, (u32)log2((float)max(x, y)) + 1U);

        // Does not support other types as of so far
        assert(desc.format != TextureFormat::GED_FORMAT_UNDEFINED);

        Texture* tex = gfx_dev.CreateTexture(desc, data);

        return tex;
    }
}
