#pragma once
#include <map>
#include <vector>
#include "../core/common.h"
#include "../core/gedmath.h"
#include "../core/stringid.h"
#include "../core/resource.h"
#include "../geom/aabb.h"
#include "vertex.h"
#include "index.h"

namespace ged
{
    class Model;
    class GraphicsDevice;

	class Mesh : public ResourceBase
	{
	public:
        Mesh(const VertexFormatDescriptor& vfd, const IndexFormatDescriptor& ifd) :
            m_vertex_format_descriptor(vfd),
            m_index_format_descriptor(ifd),
            m_total_bone_hash(0)
            {}

		struct Group
		{
            Group(u32 offset = 0, u32 length = 0) :
                offset(offset), length(length)
            {}

			u32 offset;
			u32 length;
		};

        virtual void SetVertexData(const void* data, size_t byte_size) = 0;
		virtual void* MapVertexData() = 0;
        virtual void UnmapVertexData() = 0;

        virtual void SetIndexData(const void* data, size_t byte_size) = 0;
        virtual void* MapIndexData() = 0;
        virtual void UnmapIndexData() = 0;

        u32 GetTotalBoneHash() const;

        const std::vector<i32>& GetBoneParents() const;
		const std::vector<mat4>& GetBoneOffsets() const;
		size_t GetBoneSize() const;
		i32 GetBoneIndex(const StringID& sid) const;

		const std::vector<Group>& GetGroups() const;
        const std::vector<AABB>& GetGroupAABBs() const;
		size_t GetGroupSize() const;
		i32 GetGroupIndex(const StringID& sid) const;

        const VertexFormatDescriptor& GetVertexFormatDescriptor() const;
        const IndexFormatDescriptor& GetIndexFormatDescriptor() const;

    protected:
        std::vector<i32>& GetBoneParents();
        std::vector<mat4>& GetBoneOffsets();
        std::map<StringID, i32>& GetBoneMap();

        std::vector<Group>& GetGroups();
        std::vector<AABB>& GetGroupAABBs();
        std::map<StringID, i32>& GetGroupMap();
	private:
        friend Mesh* LoadMesh(const GraphicsDevice& gfx_dev, const char* filename);

        const VertexFormatDescriptor m_vertex_format_descriptor;
        const IndexFormatDescriptor m_index_format_descriptor;

        u32 m_total_bone_hash;

        std::vector<i32> m_bone_parents;
        std::vector<mat4> m_bone_offsets;
		std::map<StringID, i32> m_bone_name_map;

		std::vector<Group> m_groups;
        std::vector<AABB> m_group_aabb;
		std::map<StringID, i32> m_group_name_map;
	};

    Mesh* LoadMesh(const GraphicsDevice& gfx_dev, const char* filename);
    inline Mesh* LoadMesh(const GraphicsDevice& gfx_dev, const std::string& filename)
    {
        return LoadMesh(gfx_dev, filename.c_str());
    }

    inline u32 Mesh::GetTotalBoneHash() const
    {
        return m_total_bone_hash;
    }

    inline const std::vector<i32>& Mesh::GetBoneParents() const
    {
        return m_bone_parents;
    }

    inline const std::vector<mat4>& Mesh::GetBoneOffsets() const
    {
        return m_bone_offsets;
    }

	inline size_t Mesh::GetBoneSize() const
	{
		return m_bone_parents.size();
	}

	inline i32 Mesh::GetBoneIndex(const StringID& sid) const
	{
		auto it = m_bone_name_map.find(sid);
		if (it != m_bone_name_map.end())
			return it->second;
		return -1;
	}

	inline const std::vector<Mesh::Group>& Mesh::GetGroups() const
	{
		return m_groups;
	}

    inline const std::vector<AABB>& Mesh::GetGroupAABBs() const
    {
        return m_group_aabb;
    }

	inline size_t Mesh::GetGroupSize() const
	{
		return m_groups.size();
	}

	inline i32 Mesh::GetGroupIndex(const StringID& sid) const
	{
		auto it = m_group_name_map.find(sid);
		if (it != m_group_name_map.end())
			return it->second;
		return -1;
	}

    inline const VertexFormatDescriptor& Mesh::GetVertexFormatDescriptor() const
    {
        return m_vertex_format_descriptor;
    }

    inline const IndexFormatDescriptor& Mesh::GetIndexFormatDescriptor() const
    {
        return m_index_format_descriptor;
    }

    inline std::vector<Mesh::Group>& Mesh::GetGroups()
    {
        return m_groups;
    }

    inline std::vector<AABB>& Mesh::GetGroupAABBs()
    {
        return m_group_aabb;
    }

    inline std::map<StringID, i32>& Mesh::GetGroupMap()
    {
        return m_group_name_map;
    }

    inline std::vector<i32>& Mesh::GetBoneParents()
    {
        return m_bone_parents;
    }

    inline std::vector<mat4>& Mesh::GetBoneOffsets()
    {
        return m_bone_offsets;
    }

    inline std::map<StringID, i32>& Mesh::GetBoneMap()
    {
        return m_bone_name_map;
    }
}
