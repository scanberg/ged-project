#include "model.h"

#include <algorithm>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include "core/iohelper.h"
#include "graphics/graphicsdevice.h"
#include "graphics/mesh.h"
#include "graphics/texture.h"
#include "material.h"

namespace ged
{
    void Model::MapMaterialToAllGroups(Material* material)
    {
        if (!m_mesh) {
            printf("Error: Could not assign material to group, no valid mesh assigned.\n");
            return;
        }

        m_grp_mat_mappings.clear();
        m_grp_mat_mappings.reserve(m_grp_mat_mappings.size());
        for (size_t i = 0; i < m_mesh->GetGroupSize(); i++)
            m_grp_mat_mappings.emplace_back(i, material);

        UpdateAABB();
    }

    void Model::MapMaterialToGroupIdx(Material* material, size_t idx)
    {
        if (!m_mesh) {
            printf("Error: Could not assign material to group, no valid mesh assigned.\n");
            return;
        }

        if (idx >= m_mesh->GetGroupSize()) {
            printf("Error: Group index out of range.\n");
            return;
        }

        // If mapping already exists, remap the material
        for (auto& grp_mat_map : m_grp_mat_mappings) {
            if (grp_mat_map.group_idx == idx) {
                grp_mat_map.material = material;
                return;
            }

            // Possible early break since sorted
            if (grp_mat_map.group_idx > idx)
                break;
        }

        // Mapping did not exist, insert and keep sorted
        m_grp_mat_mappings.emplace_back(idx, material);
        std::sort(m_grp_mat_mappings.begin(), m_grp_mat_mappings.end());

        UpdateAABB();
    }

    void Model::MapMaterialToGroup(Material* material, const StringID& id)
    {
        i32 idx = GetMesh()->GetGroupIndex(id);
        if (idx > -1)
            MapMaterialToGroupIdx(material, static_cast<size_t>(idx));
    }

    void Model::UpdateAABB()
    {
        if (!m_mesh || m_grp_mat_mappings.size() == 0) {
            m_aabb = AABB();
            return;
        }

        const std::vector<AABB>& aabbs = m_mesh->GetGroupAABBs();
        m_aabb.min_box = vec3(FLT_MAX);
        m_aabb.max_box = vec3(-FLT_MAX);

        for (const auto& grp_map : m_grp_mat_mappings) {
            if (grp_map.group_idx >= aabbs.size())
                continue;

            const AABB& grp_aabb = aabbs[grp_map.group_idx];
            m_aabb.min_box = glm::min(m_aabb.min_box, grp_aabb.min_box);
            m_aabb.max_box = glm::max(m_aabb.max_box, grp_aabb.max_box);
        }
    }

    Model* LoadModel(const GraphicsDevice& gfx_dev, const char* filename)
    {
        //FIXME std::getline, unlike std::ifstream::getline, reads unformatted text, which leaves line break characters in the readout strings.
        using std::getline;

        assert(filename);

        std::ifstream file(filename, std::ifstream::in);
        if (!file.is_open())
        {
            printf("There was a problem opening the model file: '%s' \n", filename);
            return nullptr;
        }

        std::string path = GetPath(filename);

        std::string line;
        std::vector<Material*> materials;
        std::map<std::string, size_t> mat_idx_map;
        std::map<std::string, std::string> grp_mat_map;

        Mesh* mesh = nullptr;

        while (file.good() && !file.eof()) {
            getline(file,line);
            if (line[0] == '#' || line[0] == '\n')
                continue;

            std::stringstream ss(line);
            std::string word;
            ss >> word;

            if (word == "MESH_FILE")
            {
                std::string mesh_file;
                ss >> mesh_file;
                mesh = LoadMesh(gfx_dev, (path + mesh_file).c_str());
            }
            else if (word == "MATERIAL_FILE")
            {
                std::string material_file;
                ss >> material_file;
                materials = LoadMaterials(gfx_dev, (path + material_file).c_str(), &mat_idx_map);
            }
            else if (word == "GROUP_MATERIAL")
            {
                std::string group;
                std::string material;

                ss >> group;
                ss >> material;

                grp_mat_map[group] = material;
            }
        }

        file.close();

        Model* mdl = new Model();
        mdl->SetMesh(mesh);

        // Map materials to groups
        if (materials.size() > 0) {
            for (auto grp_mat_pair : grp_mat_map)
            {
                const std::string& grp = grp_mat_pair.first;
                const std::string& mat = grp_mat_pair.second;

                auto mat_idx = mat_idx_map.find(mat);
                if (mat_idx != mat_idx_map.end())
                    mdl->MapMaterialToGroup(materials[mat_idx->second], SID(grp));
                else
                    printf("could not find a match for %s \n", grp.c_str());
            }
        }

        return mdl;
    }
}
