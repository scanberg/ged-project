# [GET/GED Framework](https://bitbucket.org/scanberg/get-framework)

## Dependencies
GET/GED Framework uses the following dependencies (already included in the source package as git submodules).

Functionality             | Library
------------------------- | ------------------------------------------
OpenGL Function Loader    | [glad](https://github.com/Dav1dde/glad)
Windowing and Input       | [glfw](https://github.com/glfw/glfw)
OpenGL Mathematics        | [glm](https://github.com/g-truc/glm)
Texture Loading           | [stb](https://github.com/nothings/stb)

To pull all submodules from remote do

```git submodule update --init
```

or use the `--recursive` when cloning the repository.